import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:thriftr/src/ui/application.dart';

void main() {
  runApp(Application());
  _setupLog();
}

void _setupLog() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen(
    (data) {
      print("${data.level.name} : ${data.time} : ${data.message}");
    },
  );
}