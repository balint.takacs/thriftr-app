class EmptyTokenException implements Exception {
  String message = "empty_token";

  @override
  String toString() {
    return message;
  }
}
