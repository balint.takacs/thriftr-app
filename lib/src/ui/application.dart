import 'package:auto_route/auto_route.dart' as autoRoute;
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:thriftr/src/providers/authentication/authentication_provider.dart';
import 'package:thriftr/src/providers/product/product_provider.dart';
import 'package:thriftr/src/providers/shop/shop_provider.dart';
import 'package:thriftr/src/providers/user/user_provider.dart';
import 'package:thriftr/src/resources/style.dart';
import 'package:thriftr/src/resources/routes/application_routes.dart' as routes;

class Application extends StatefulWidget {
  Application({Key key}) : super(key: key);

  @override
  _ApplicationState createState() => _ApplicationState();
}

class _ApplicationState extends State<Application> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => AuthenticationProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => ShopProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => ProductProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => UserProvider(),
        ),
      ],
      child: DefaultTextStyle(
        style: TextStyle(
          fontSize: 25,
        ),
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ApplicationStyle.themeData,
          builder: autoRoute.ExtendedNavigator.builder<routes.Router>(
            router: routes.Router(),
          ),
        ),
      ),
    );
  }
}
