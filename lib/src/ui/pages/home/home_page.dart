import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:provider/provider.dart';
import 'package:thriftr/src/providers/user/user_provider.dart';
import 'package:thriftr/src/resources/route_helper.dart';
import 'package:thriftr/src/resources/routes/application_routes.dart';
import 'package:thriftr/src/ui/pages/bussines/edit_bussines_page.dart';
import 'package:thriftr/src/ui/pages/favorites/favorites_page.dart';
import 'package:thriftr/src/ui/pages/feed/feed_page.dart';
import 'package:thriftr/src/ui/pages/search/search_page.dart';
import 'package:thriftr/src/ui/widget/bottom/custom_bottom_tab_navigator.dart';
import 'package:thriftr/src/ui/widget/logo/custom_logo.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _pageCount = 0;
  PersistentTabController _persistentTabController;

  List<Widget> _pages = [
    FeedPage(),
    SearchPage(),
    EditBussinesPage(),
    FavoritesPage(),
  ];

  //
  // Widget get buildBody {
  //   return PageView(
  //     controller: _pageController,
  //     physics: BouncingScrollPhysics(),
  //     children: _pages,
  //     onPageChanged: (page) {
  //       setState(() {
  //         _pageCount = page;
  //         build(context);
  //       });
  //     },
  //   );
  // }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _persistentTabController = new PersistentTabController(
      initialIndex: 0,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: PersistentTabView.custom(
        context,
        controller: _persistentTabController,
        screens: _pages,
        hideNavigationBarWhenKeyboardShows: true,
        handleAndroidBackButtonPress: true,
        confineInSafeArea: true,
        itemCount: 4,
        customWidget: CustomBottomTabNavigator(
          onPageChanged: (page) {
            setState(() {
              _pageCount = page;
              build(context);
            });
          },
          children: [
            CustomTabNavigatorItem(
              title: "👕",
              icon: Feather.home,
              fullIcon: Feather.home,
              selected: _pageCount == 0,
              onPressed: () {
                _persistentTabController.jumpToTab(0);

                setState(() {
                  _pageCount = 0;
                  build(context);
                });

                Navigator.of(context).popUntil((route) => route.isFirst);
              },
            ),
            CustomTabNavigatorItem(
              title: "📩",
              icon: Feather.search,
              fullIcon: Feather.search,
              selected: _pageCount == 1,
              onPressed: () {
                _persistentTabController.jumpToTab(1);
                setState(() {
                  _pageCount = 1;
                  build(context);
                });
                Navigator.of(context).popUntil((route) => route.isFirst);
              },
            ),
            CustomTabNavigatorItem(
              icon: Icons.add,
              selected: _pageCount == 2,
              mainAction: true,
              onPressed: () {
                Navigator.of(context).popUntil((route) => route.isFirst);
                ExtendedNavigator.of(context).push(Routes.createBussinesPage);
                /* if (Provider.of<UserProvider>(context, listen: false)
                  .currentUser
                  .hasShop) {

              } else {
                ExtendedNavigator.of(context).push(Routes.createBussinesPage);
              }*/
              },
            ),
            CustomTabNavigatorItem(
              title: "👤",
              icon: Feather.briefcase,
              fullIcon: Icons.person,
              selected: _pageCount == 2,
              onPressed: () {
                _persistentTabController.jumpToTab(2);
                setState(() {
                  _pageCount = 2;
                  build(context);
                });
                Navigator.of(context).popUntil((route) => route.isFirst);
              },
            ),
            CustomTabNavigatorItem(
              title: "⚙️",
              icon: Feather.bookmark,
              fullIcon: MaterialCommunityIcons.bookmark,
              selected: _pageCount == 3,
              onPressed: () {
                _persistentTabController.jumpToTab(3);
                setState(() {
                  _pageCount = 3;
                  build(context);
                });
                Navigator.of(context).popUntil((route) => route.isFirst);
              },
            ),
          ],
        ),
        screenTransitionAnimation: ScreenTransitionAnimation(
          animateTabTransition: true,
          curve: Curves.ease,
          duration: Duration(milliseconds: 200),
        ),
      ),
    );
  }
}
