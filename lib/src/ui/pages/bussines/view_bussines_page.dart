import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:thriftr/src/models/product/product_model.dart';
import 'package:thriftr/src/models/shop/shop_model.dart';
import 'package:thriftr/src/providers/shop/shop_provider.dart';
import 'package:thriftr/src/repositories/shop_repository.dart';
import 'package:thriftr/src/resources/modal_helper.dart';
import 'package:thriftr/src/resources/style.dart';
import 'package:thriftr/src/ui/widget/button/custom_button.dart';
import 'package:thriftr/src/ui/widget/button/custom_flat_button.dart';
import 'package:thriftr/src/ui/widget/custom_headline.dart';
import 'package:thriftr/src/ui/widget/custom_stars.dart';
import 'package:thriftr/src/ui/widget/logo/custom_logo.dart';
import 'package:thriftr/src/ui/widget/product/custom_product_grid_tile.dart';

class ViewBussinesPage extends StatefulWidget {
  ShopModel shopModel;

  ViewBussinesPage({
    Key key,
    this.shopModel,
  }) : super(key: key);

  @override
  _ViewBussinesPageState createState() => _ViewBussinesPageState();
}

class _ViewBussinesPageState extends State<ViewBussinesPage> {
  List<ProductModel> shopProducts;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(
      (timeStamp) {
        Provider.of<ShopProvider>(context, listen: false)
            .fetchShopDetails(
          shopId: widget.shopModel.id,
        )
            .then(
          (value) {
            if (value != null) {
              print("ShopModel: $value");
              setState(() {
                widget.shopModel = value;
              });
            }
          },
        );
        Provider.of<ShopProvider>(context, listen: false).fetchShopProducts(
          shopId: widget.shopModel.id,
        );
      },
    );
  }

  Future<void> follow() async {
    if (Provider.of<ShopProvider>(context, listen: false).isLoading) {
      return;
    }
    if (widget.shopModel.follow) {
      bool go = await ModalHelper.showPopUp(
        context,
        title: Text("Are you sure? 🤔"),
        content: Column(
          children: [
            Text("Are you sure to unfollow ${widget.shopModel.name} ?"),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomButton(
                  child: Text("Yes"),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                ),
                SizedBox(width: 5),
                CustomButton(
                  child: Text("No"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ],
        ),
      );
      if (go == null) {
        return;
      }
      Exception error = await Provider.of<ShopProvider>(context, listen: false)
          .unFollowShop(shopId: widget.shopModel.id);

      if (error == null) {
        setState(() {
          widget.shopModel = widget.shopModel.rebuild(
            (d) => d
              ..follow = false
              ..followers = widget.shopModel.followers - 1,
          );
        });
      }
    } else {
      Exception error = await Provider.of<ShopProvider>(context, listen: false)
          .followShop(shopId: widget.shopModel.id);

      if (error == null) {
        setState(() {
          widget.shopModel = widget.shopModel.rebuild(
            (d) => d
              ..follow = true
              ..followers = widget.shopModel.followers + 1,
          );
        });
      }
    }
  }

  Widget get buildTitle {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Divider(),
        Card(
          shadowColor: Colors.black,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              5,
            ),
          ),
          child: Padding(
            padding: EdgeInsets.all(3),
            child: Container(
              width: 70,
              height: 70,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(
                  3,
                ),
                child: Image.network(
                  "${widget.shopModel.avatar}",
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Opacity(
          opacity: 1,
          child: Text(
            "${widget.shopModel.name}",
            style: TextStyle(
              fontFamily: "Montserrat",
              fontSize: 13,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
        ),
        CustomStars(
          color: Colors.white,
          count: 3,
          size: 10,
        ),
        Text(
          "( 5 )",
          style: TextStyle(
            fontFamily: "Montserrat",
            fontSize: 10,
            fontWeight: FontWeight.w200,
            color: Colors.white,
          ),
        ),
        SizedBox(height: 10),
        widget.shopModel.follow == null
            ? Container()
            : InkWell(
                onTap: () {
                  follow();
                },
                child: Container(
                  padding: EdgeInsets.symmetric(
                    vertical: 5,
                    horizontal: 10,
                  ),
                  color: ApplicationStyle.primaryColor,
                  child: Text(
                    !widget.shopModel.follow ? "Follow" : "Unfollow",
                    style: TextStyle(
                      fontFamily: "Montserrat",
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
      ],
    );
  }

  Widget buildColumnText({String title, String value}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          value,
          style: TextStyle(
            color: ApplicationStyle.primaryColor,
            fontWeight: FontWeight.bold,
          ),
        ),
        Text(
          title,
          style: TextStyle(
            color: Colors.black26,
            fontSize: 10,
          ),
        ),
      ],
    );
  }

  Widget get buildFollowerInfo {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            buildColumnText(
                title: "Followers", value: "${widget.shopModel.followers}"),
            Container(
              width: 1,
              height: 15,
              color: Colors.black.withOpacity(0.1),
            ),
            buildColumnText(title: "Sold", value: "50"),
            Container(
              width: 1,
              height: 15,
              color: Colors.black.withOpacity(0.1),
            ),
            buildColumnText(title: "Rating", value: "4.7"),
          ],
        ),
        Divider(),
      ],
    );
  }

  bool _imageLoadError = false;
  Widget get buildHeader {
    return Stack(
      children: [
        Positioned.fill(
          child: Container(
            child: new BackdropFilter(
              filter: new ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
              child: new Container(
                decoration: new BoxDecoration(
                  color: Colors.black.withOpacity(0.4),
                ),
              ),
            ),
            decoration: BoxDecoration(
              image: new DecorationImage(
                image: new NetworkImage(
                  "${widget.shopModel.cover}",
                ),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        Positioned.fill(
          child: Center(
            child: buildTitle,
          ),
        )
      ],
    );
  }

  Widget get buildBio {
    return Container(
      width: double.maxFinite,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomHeadline(title: "Bio"),
          Divider(),
          Text("${widget.shopModel.description}")
        ],
      ),
    );
  }

  Widget get buildProducts {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomHeadline(title: "Products"),
        shopProducts == null
            ? Text("Loading")
            : GridView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: shopProducts.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisSpacing: 10,
                  childAspectRatio: 0.65,
                  crossAxisSpacing: 10,
                ),
                itemBuilder: (context, count) {
                  return CustomProductGridTile(
                    productModel: shopProducts[count],
                  );
                },
              ),
      ],
    );
  }

  Widget get buildBody {
    return CustomScrollView(
      physics: BouncingScrollPhysics(),
      slivers: [
        SliverAppBar(
          expandedHeight: 200,
          centerTitle: true,
          backgroundColor: Colors.white,
          actions: [
            FloatingActionButton(
              heroTag: 'btn1',
              mini: true,
              backgroundColor: Colors.white,
              child: Icon(
                Icons.more_horiz,
                color: Colors.black,
              ),
              onPressed: () {
                ModalHelper.showCustomModal(
                  context,
                  title: Text("Actions"),
                  content: Column(
                    children: [
                      ListTile(
                        leading: Icon(
                          Icons.report,
                        ),
                        title: Text("Report"),
                      ),
                    ],
                  ),
                );
              },
            ),
          ],
          leadingWidth: 50,
          leading: Padding(
            padding: EdgeInsets.all(5),
            child: FloatingActionButton(
              mini: true,
              backgroundColor: Colors.white,
              child: Icon(
                Icons.close,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ),
          floating: true,
          flexibleSpace: FlexibleSpaceBar(
            background: buildHeader,
            centerTitle: true,
            titlePadding: EdgeInsets.all(0),
          ),
        ),
        SliverAppBar(
          floating: true,
          pinned: true,
          automaticallyImplyLeading: false,
          centerTitle: true,
          backgroundColor: Color(0xFFF2F2F2),
          title: buildFollowerInfo,
        ),
        SliverPadding(
          padding: EdgeInsets.all(ApplicationStyle.paddingValue),
          sliver: SliverList(
            delegate: SliverChildListDelegate.fixed(
              [
                buildBio,
                Divider(),
                buildProducts,
              ],
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    shopProducts =
        Provider.of<ShopProvider>(context, listen: true).shopProducts;

    return Scaffold(
      body: buildBody,
    );
  }
}
