import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';
import 'package:thriftr/src/models/shop/shop_model.dart';
import 'package:thriftr/src/providers/shop/shop_provider.dart';
import 'package:thriftr/src/resources/extensions.dart';
import 'package:thriftr/src/resources/style.dart';
import 'package:thriftr/src/ui/widget/bussines/custom_bussines_listtile.dart';
import 'package:thriftr/src/ui/widget/bussines/custom_min_bussines_tile.dart';
import 'package:thriftr/src/ui/widget/custom_headline.dart';
import 'package:thriftr/src/ui/widget/custom_stars.dart';

class AllBussinesPage extends StatefulWidget {
  AllBussinesPage({Key key}) : super(key: key);

  @override
  _AllBussinesPageState createState() => _AllBussinesPageState();
}

class _AllBussinesPageState extends State<AllBussinesPage> {
  List<ShopModel> allShops;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(
      (timeStamp) {
        Provider.of<ShopProvider>(context, listen: false).fetchAllShops();
      },
    );
  }

  Widget get buildBody {
    return CustomScrollView(
      physics: BouncingScrollPhysics(),
      slivers: [
        SliverAppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          floating: true,
          expandedHeight: 100,
          toolbarHeight: 100,
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  if (Navigator.of(context).canPop()) {
                    Navigator.of(context).pop();
                  }
                },
                child: Icon(
                  Icons.close,
                  color: ApplicationStyle.primaryColor,
                ),
              ),
              Divider(),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 0),
                child: Row(
                  children: [
                    CustomHeadline(title: "Shops"),
                    Spacer(),
                    InkWell(
                      child: Icon(
                        MaterialCommunityIcons.filter_outline,
                        color: ApplicationStyle.primaryColor,
                      ),
                    ),
                  ],
                ),
              ),
              Divider(),
            ],
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate(
            allShops == null
                ? [
                    Text("Loading"),
                  ]
                : allShops.map((e) => CustomBussinesListTile(shopModel: e)).toList(),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    allShops = Provider.of<ShopProvider>(context, listen: true).allShops;
    return Scaffold(
      body: RefreshIndicator(
        child: buildBody,
        onRefresh: (){
          print("Reefresh");
          return Future.value(true);
        },
      ),
    );
  }
}
