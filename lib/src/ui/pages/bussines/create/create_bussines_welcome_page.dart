import 'package:flutter/material.dart';
import 'package:thriftr/src/ui/widget/button/custom_button.dart';

class CreateBussinesWelcomePage extends StatefulWidget {
  final VoidCallback onStart;
  CreateBussinesWelcomePage({
    Key key,
    this.onStart,
  }) : super(key: key);

  @override
  _CreateBussinesWelcomePageState createState() =>
      _CreateBussinesWelcomePageState();
}

class _CreateBussinesWelcomePageState extends State<CreateBussinesWelcomePage> {
  Widget get buildBody {
    return Stack(
      children: [
        Positioned(
          left: 0,
          right: 0,
          top: 0,
          height: MediaQuery.of(context).size.height * 0.6,
          child: Stack(
            children: [
              Positioned.fill(
                child: Image.asset(
                  "assets/intro/intro_first.jpg",
                  fit: BoxFit.cover,
                ),
              ),
              Positioned(
                bottom: 0,
                right: 0,
                left: 0,
                height: MediaQuery.of(context).size.height * 0.5,
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.transparent,
                        Colors.white30,
                        Colors.white,
                      ],
                      end: Alignment.bottomCenter,
                      begin: Alignment.topCenter,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Positioned(
          left: 10,
          top: 10,
          child: SafeArea(
            child: CloseButton(),
          ),
        ),
        Positioned.fill(
          child: SafeArea(
            minimum: EdgeInsets.all(
              20,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Készen állsz?",
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 25),
                ),
                Divider(),
                Text(
                  "Teremts divatot. Add el felesleges ruháidat. Engedd szabadjára kreativításod a Thriftr segítségével!",
                ),
                Divider(),
                Text(
                  "Teremts divatot. Add el felesleges ruháidat. Engedd szabadjára kreativításod a Thriftr segítségével!",
                ),
                Divider(),
                Padding(
                  padding: EdgeInsets.all(10),
                  child: CustomButton(
                    onPressed: () {
                      if(widget.onStart != null){
                        widget.onStart();
                      }
                    },
                    maxWidth: true,
                    child: Text("Kezdjük!"),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildBody,
    );
  }
}
