import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:thriftr/src/models/user/user_model.dart';
import 'package:thriftr/src/providers/user/user_provider.dart';
import 'package:thriftr/src/resources/modal_helper.dart';
import 'package:thriftr/src/resources/style.dart';
import 'package:thriftr/src/ui/pages/bussines/create/create_bussines_welcome_page.dart';
import 'package:thriftr/src/ui/widget/button/custom_button.dart';
import 'package:thriftr/src/ui/widget/custom_headline.dart';
import 'package:thriftr/src/ui/widget/custom_stars.dart';
import 'package:thriftr/src/ui/widget/logo/custom_logo.dart';

class CreateBussinesPage extends StatefulWidget {
  CreateBussinesPage({Key key}) : super(key: key);

  @override
  _CreateBussinesPageState createState() => _CreateBussinesPageState();
}

class _CreateBussinesPageState extends State<CreateBussinesPage> {
  File _avatarImage;
  File _coverImage;

  final TextEditingController _shopTitleController =
      new TextEditingController();
  final TextEditingController _shopBioController = new TextEditingController();

  final FocusNode _titleFocusNode = new FocusNode();
  final FocusNode _bioFocusNode = new FocusNode();

  final PageController _pageController = new PageController();

  int _currentPage = 0;

  UserModel userModel;

  bool _welcomePageVisible = true;

  Widget get buildTitleTextField {
    return Padding(
      padding: EdgeInsets.all(20),
      child: TextField(
        focusNode: _titleFocusNode,
        controller: _shopTitleController,
        textAlignVertical: TextAlignVertical.center,
        onSubmitted: (d) {
          FocusScope.of(context).requestFocus(_bioFocusNode);
        },
        maxLength: 20,
        decoration: InputDecoration(
          hintText: "Name",
          prefixIcon: Icon(
            Icons.add_business_outlined,
            color: ApplicationStyle.primaryColor,
          ),
        ),
      ),
    );
  }

  Widget get buildBioTextField {
    return Padding(
      padding: EdgeInsets.all(20),
      child: TextField(
        focusNode: _bioFocusNode,
        controller: _shopBioController,
        textAlignVertical: TextAlignVertical.center,
        maxLength: 160,
        maxLines: 5,
        onEditingComplete: () async {
          FocusScope.of(context).requestFocus(new FocusNode());
          await Future.delayed(
            Duration(
              milliseconds: 100,
            ),
          );

          _pageController.jumpToPage(
            1,
          );
        },
        onSubmitted: (d) async {
          FocusScope.of(context).requestFocus(new FocusNode());
          await Future.delayed(
            Duration(
              milliseconds: 100,
            ),
          );

          _pageController.jumpToPage(
            1,
          );
        },
        decoration: InputDecoration(
          hintText: "Bio",
          prefixIconConstraints: BoxConstraints(
            maxHeight: 105,
            maxWidth: 50,
          ),
          prefixIcon: Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: EdgeInsets.only(
                left: 10,
              ),
              child: Icon(
                Icons.add_business_outlined,
                color: ApplicationStyle.primaryColor,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildInformationsText({String information}) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 20,
      ),
      child: Text(
        information,
        textAlign: TextAlign.justify,
        style: TextStyle(
          color: Colors.black26,
          fontSize: 15,
        ),
      ),
    );
  }

  Widget get buildInformations {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Column(
        children: [
          buildTitleTextField,
          buildInformationsText(
            information:
                "A névnek egyedinek kell lennie, egybe írva.\nTipp: használd azt a nevet, amelyez a közösségi platformokon is használsz.",
          ),
          buildBioTextField,
          buildInformationsText(
            information:
                "Használd a betűk erejét, mutasd be a boltodat mindössze 160 karakter betűvel. Menni fog!",
          ),
        ],
      ),
    );
  }

  Widget get buildShopDesignTitle {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Divider(),
        Card(
          shadowColor: Colors.black,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              5,
            ),
          ),
          child: Padding(
            padding: EdgeInsets.all(3),
            child: Container(
              width: 70,
              height: 70,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(
                  3,
                ),
                child: _avatarImage == null
                    ? Icon(Icons.add)
                    : Image.file(
                        _avatarImage,
                        fit: BoxFit.cover,
                      ),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Opacity(
          opacity: 1,
          child: Text(
            "${_shopTitleController.text}",
            style: TextStyle(
              fontFamily: "Montserrat",
              fontSize: 13,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
        ),
        CustomStars(
          color: Colors.white,
          count: 3,
          size: 10,
        ),
      ],
    );
  }

  Widget get buildFakeProducts {
    Widget fake = Container(
      child: CustomLogo(),
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.black,
        ),
      ),
    );
    return Stack(
      children: [
        Positioned.fill(
          child: GridView.count(
            physics: NeverScrollableScrollPhysics(),
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            shrinkWrap: true,
            crossAxisCount: 3,
            children:
                List.generate(30, (index) => null).map((e) => fake).toList(),
          ),
        ),
        Positioned(
          bottom: 0,
          right: 0,
          left: 0,
          height: 500,
          child: Container(
            width: double.maxFinite,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Colors.white,
                  Colors.white70,
                  Colors.transparent,
                ],
                end: Alignment.topCenter,
                begin: Alignment.bottomCenter,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget get buildDesignShop {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: double.maxFinite,
          height: 200,
          child: Stack(
            children: [
              Positioned(
                top: 0,
                right: 0,
                child: IconButton(
                  icon: Icon(
                    Icons.add,
                  ),
                ),
              ),
              Positioned.fill(
                child: _coverImage == null
                    ? Container(
                        color: Colors.black26,
                      )
                    : Container(
                        child: new BackdropFilter(
                          filter:
                              new ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
                          child: new Container(
                            decoration: new BoxDecoration(
                              color: Colors.black.withOpacity(0.4),
                            ),
                          ),
                        ),
                        decoration: BoxDecoration(
                          image: new DecorationImage(
                            image: new FileImage(
                              _coverImage,
                            ),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
              ),
              Positioned.fill(
                child: buildShopDesignTitle,
              ),
            ],
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.all(
              ApplicationStyle.paddingValue,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomHeadline(
                  title: "Bio",
                ),
                Divider(),
                Text(
                  "${_shopBioController.text}",
                ),
                Divider(),
                CustomHeadline(
                  title: "Products",
                ),
                Divider(),
                Expanded(
                  child: buildFakeProducts,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget get buildBody {
    if (!_welcomePageVisible) {
      return PageView(
        physics: BouncingScrollPhysics(),
        onPageChanged: (page) {
          setState(() {
            _currentPage = page;
          });
        },
        controller: _pageController,
        children: [
          buildInformations,
          buildDesignShop,
        ],
      );
    }
    return CreateBussinesWelcomePage(
      onStart: () {
        setState(() {
          _welcomePageVisible = false;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    userModel = Provider.of<UserProvider>(context, listen: true).currentUser;
    return Scaffold(
      appBar: _welcomePageVisible
          ? null
          : AppBar(
              automaticallyImplyLeading: false,
              actions: [
                IconButton(
                  icon: Icon(
                    _currentPage != 1 ? Icons.arrow_forward : Icons.check,
                  ),
                  onPressed: () {
                    if (_currentPage != 1) {
                      _pageController.nextPage(
                        duration: Duration(milliseconds: 200),
                        curve: Curves.ease,
                      );
                    } else {}
                  },
                )
              ],
              bottom: PreferredSize(
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: EdgeInsets.all(
                      20,
                    ),
                    child: CustomHeadline(
                      title: _currentPage == 1
                          ? "How it look?"
                          : "Create your Shop",
                    ),
                  ),
                ),
                preferredSize: Size.fromHeight(50),
              ),
              leading: _currentPage == 0
                  ? CloseButton()
                  : IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                      ),
                      onPressed: () {
                        _pageController.previousPage(
                          duration: Duration(milliseconds: 200),
                          curve: Curves.ease,
                        );
                      },
                    ),
            ),
      body: buildBody,
    );
  }
}
