import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:thriftr/src/models/product/brand/product_brand_model.dart';
import 'package:thriftr/src/models/product/product_model.dart';
import 'package:thriftr/src/models/shop/shop_model.dart';
import 'package:thriftr/src/providers/product/product_provider.dart';
import 'package:thriftr/src/providers/shop/shop_provider.dart';
import 'package:thriftr/src/resources/routes/application_routes.dart';
import 'package:thriftr/src/resources/style.dart';
import 'package:thriftr/src/ui/widget/brand/custom_min_brand_tile.dart';
import 'package:thriftr/src/ui/widget/bussines/custom_min_bussines_tile.dart';
import 'package:thriftr/src/ui/widget/button/custom_flat_button.dart';
import 'package:thriftr/src/ui/widget/custom_headline.dart';
import 'package:thriftr/src/ui/widget/custom_underline_headline.dart';
import 'package:thriftr/src/ui/widget/feed/custom_feed_tile.dart';
import 'package:thriftr/src/ui/widget/logo/custom_logo.dart';
import 'package:thriftr/src/ui/widget/product/custom_min_product_tile.dart';
import 'package:thriftr/src/ui/widget/product/custom_product_grid_tile.dart';

class FeedPage extends StatefulWidget {
  FeedPage({Key key}) : super(key: key);

  @override
  _FeedPageState createState() => _FeedPageState();
}

class _FeedPageState extends State<FeedPage> {
  List<ProductModel> mostPopularProducts;
  List<ProductBrandModel> mostPopularBrands;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _initFeed();
    });
  }

  Future<void> _initFeed() async {
    Provider.of<ProductProvider>(context, listen: false)
        .fetchMostPopularProducts();
    Provider.of<ProductProvider>(context, listen: false).fetchProductDatas();
  }

  Widget get buildTopSearch {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.all(
            ApplicationStyle.paddingValue,
          ),
          child: CustomHeadline(
            title: "Top Searches",
          ),
        ),
        SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              SizedBox(
                width: 10,
              ),
              CustomFlatButton(
                child: Text(
                  "Adidas Hoodie",
                ),
              ),
              SizedBox(
                width: 10,
              ),
              CustomFlatButton(
                child: Text(
                  "Adidas Hoodie",
                ),
              ),
              SizedBox(
                width: 10,
              ),
              CustomFlatButton(
                child: Text(
                  "Adidas Hoodie",
                ),
              ),
              SizedBox(
                width: 10,
              ),
              CustomFlatButton(
                child: Text(
                  "Adidas Hoodie",
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget get buildPopularProducts {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.all(
            ApplicationStyle.paddingValue,
          ),
          child: CustomHeadline(
            title: "Most Popular Products",
          ),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          physics: BouncingScrollPhysics(),
          child: Row(
            children: mostPopularProducts
                .map(
                  (e) => Container(
                    height: 200, 
                    width: 100,
                    margin: EdgeInsets.symmetric(horizontal:10,),
                    child: CustomProductGridTile(
                      productModel: e,
                    ),
                  ),
                )
                .toList(),
          ),
        ),
      ],
    );
  }

  Widget get buildBottomToMore {
    return Container(
      width: double.maxFinite,
      height: 300,
      padding: EdgeInsets.all(
        ApplicationStyle.paddingValue,
      ),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/intro/intro_first.jpg"),
          fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(
            Colors.black45,
            BlendMode.colorBurn,
          ),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "You want more?",
            style: TextStyle(
              color: Colors.white,
              fontSize: 25,
              fontWeight: FontWeight.bold,
            ),
          ),
          Divider(),
          Text(
            "Use search menu to find the best products for yourself.",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
          Divider(),
          Divider(),
          Container(
            width: 100,
            child: CustomFlatButton(
              child: Text("Search"),
              backgroundColor: Colors.white,
              onPressed: () {},
            ),
          ),
        ],
      ),
    );
  }

  Widget get buildRecentViewProducts {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.all(
            ApplicationStyle.paddingValue,
          ),
          child: CustomHeadline(
            title: "Recently Viewed",
          ),
        ),
        GridView.count(
          crossAxisCount: 2,
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          mainAxisSpacing: 10,
          childAspectRatio: 0.65,
          crossAxisSpacing: 10,
          padding: EdgeInsets.symmetric(
            horizontal: 10,
          ),
          children: mostPopularProducts
              .sublist(0, 4)
              .map(
                (e) => CustomProductGridTile(
                  productModel: e,
                ),
              )
              .toList(),
        ),
      ],
    );
  }

  Widget get buildBrands {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.all(
            ApplicationStyle.paddingValue,
          ),
          child: CustomHeadline(
            title: "Trendy Brands",
          ),
        ),
        GridView.count(
          crossAxisCount: 3,
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
          padding: EdgeInsets.symmetric(
            horizontal: 10,
          ),
          children: mostPopularBrands
              .sublist(0, 5)
              .map(
                (e) => CustomMinBrandTile(
                  productBrandModel: e,
                ),
              )
              .toList(),
        ),
      ],
    );
  }

  Widget get buildBody {
    return CustomScrollView(
      physics: BouncingScrollPhysics(),
      slivers: [
        SliverAppBar(
          centerTitle: true,
          backgroundColor: Colors.white,
          floating: true,
          title: CustomLogo(
            size: 100,
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate.fixed(
            [
              buildTopSearch,
              buildPopularProducts,
              buildRecentViewProducts,
              buildBrands,
              Divider(),
              Divider(),
              buildBottomToMore,
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    mostPopularProducts =
        Provider.of<ProductProvider>(context, listen: true).mostPopularProducts;
    mostPopularBrands =
        Provider.of<ProductProvider>(context, listen: true).productDatas.brands;

    return Scaffold(
      body: buildBody,
    );
  }
}
