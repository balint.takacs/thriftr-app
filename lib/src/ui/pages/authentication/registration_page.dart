import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:form_validator/form_validator.dart';
import 'package:thriftr/src/resources/style.dart';
import 'package:thriftr/src/ui/widget/button/custom_button.dart';
import 'package:thriftr/src/ui/widget/logo/custom_logo.dart';

class RegistrationPage extends StatefulWidget {
  RegistrationPage({Key key}) : super(key: key);

  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  final TextEditingController _emailController = new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();
  final TextEditingController _confirmPasswordController = new TextEditingController();

  final GlobalKey<FormState> _formKey = new GlobalKey();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  bool _keyboardIsVisible = false;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback(
      (timeStamp) {
        initKeyboardListener();
      },
    );
  }

  Future<void> initKeyboardListener() async {
    KeyboardVisibilityController _controller =
        new KeyboardVisibilityController();

    setState(() {
      _keyboardIsVisible = _controller.isVisible;
    });
    _controller.onChange.listen(
      (event) async {
        if (_keyboardIsVisible) {
          await Future.delayed(Duration(milliseconds: 100));
        }
        setState(() {
          _keyboardIsVisible = event;
        });
      },
    );
  }

  Future<void> tryLogin() async {
    if (_formKey.currentState.validate()) {}
  }

  Widget get buildEmailTextField {
    return TextFormField(
      controller: _emailController,
      validator: ValidationBuilder().email().build(),
      textAlignVertical: TextAlignVertical.center,
      decoration: InputDecoration(
        hintText: "E-Mail",
        prefixIcon: Padding(
          padding: const EdgeInsets.all(
            10,
          ),
          child: Icon(
            MaterialCommunityIcons.email_outline,
            color: ApplicationStyle.primaryColor,
          ),
        ),
      ),
    );
  }

  Widget get buildPasswordTextField {
    return TextFormField(
      controller: _passwordController,
      validator: ValidationBuilder().minLength(5).add((value) => _confirmPasswordController.text != _passwordController.text ? "Two passwords doesnt match!" : null).build(),
      textAlignVertical: TextAlignVertical.center,
      obscureText: true,
      decoration: InputDecoration(
        hintText: "Password",
        prefixIcon: Padding(
          padding: const EdgeInsets.all(
            10,
          ),
          child: Icon(
            MaterialCommunityIcons.key_outline,
            color: ApplicationStyle.primaryColor,
          ),
        ),
      ),
    );
  }
  
  Widget get buildRepeatPasswordTextField {
    return TextFormField(
      controller: _confirmPasswordController,
      validator: ValidationBuilder().minLength(5).add((value) => _confirmPasswordController.text != _passwordController.text ? "Two passwords doesnt match!" : null).build(),
      textAlignVertical: TextAlignVertical.center,
      obscureText: true,
      decoration: InputDecoration(
        hintText: "Confirm Password",
        prefixIcon: Padding(
          padding: const EdgeInsets.all(
            10,
          ),
          child: Icon(
            MaterialCommunityIcons.key_outline,
            color: ApplicationStyle.primaryColor,
          ),
        ),
      ),
    );
  }

  Widget get buildBody {
    return Center(
      child: Padding(
        padding: EdgeInsets.all(
          20,
        ),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomLogo(
                      size: 150,
                    ),
                    Divider(),
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 5,
                      ),
                      child: Text(
                        "Sign Up",
                        style: TextStyle(
                          fontFamily: "BreeSerif",
                          fontSize: 20,
                        ),
                      ),
                    ),
                    Divider(),
                    buildEmailTextField,
                    Divider(),
                    buildPasswordTextField,
                    Divider(),
                    buildRepeatPasswordTextField,
                    Divider(),
                    Divider(),
                    CustomButton(
                      maxWidth: true,
                      child: Text("Registration"),
                      onPressed: tryLogin,
                    ),
                  ],
                ),
              ),
              Visibility(
                visible: !_keyboardIsVisible,
                replacement: Container(),
                child: InkWell(
                  onTap: Navigator.of(context).pop,
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: "Already have an account?\n",
                      style: TextStyle(
                        color: Colors.black26,
                        fontSize: 12,
                      ),
                      children: [
                        TextSpan(
                          text: "Sign In!",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: ApplicationStyle.primaryColor,
                            fontSize: 15,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: buildBody,
    );
  }
}
