import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:form_validator/form_validator.dart';
import 'package:provider/provider.dart';
import 'package:thriftr/src/providers/authentication/authentication_provider.dart';
import 'package:thriftr/src/resources/messages_helper.dart';
import 'package:thriftr/src/resources/routes/application_routes.dart';
import 'package:thriftr/src/resources/style.dart';
import 'package:thriftr/src/ui/widget/button/custom_button.dart';
import 'package:thriftr/src/ui/widget/button/custom_text_button.dart';
import 'package:thriftr/src/ui/widget/logo/custom_logo.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _emailController = new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();

  final GlobalKey<FormState> _formKey = new GlobalKey();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

    KeyboardVisibilityController _controller =
        new KeyboardVisibilityController();

  bool _keyboardIsVisible = false;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback(
      (timeStamp) {
        initKeyboardListener();
      },
    );
  }

  @override
  void dispose() { 
    _controller.onChange.distinct();
    _controller = null;
    super.dispose();
  }

  Future<void> initKeyboardListener() async {

    setState(() {
      _keyboardIsVisible = _controller.isVisible;
    });
    _controller.onChange.listen(
      (event) async {
        if (_keyboardIsVisible) {
          await Future.delayed(Duration(milliseconds: 100));
        }
        setState(() { 
          _keyboardIsVisible = event;
        });
      },
    );
  }

  Future<void> tryLogin() async {
    if (_formKey.currentState.validate()) {
      Exception _error =
          await Provider.of<AuthenticationProvider>(context, listen: false)
              .signIn(
        SignInType.NORMAL,
        email: _emailController.text,
        password: _passwordController.text,
      );

      if (_error != null) {
        _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              MessagesHelper.getMessageByException(
                context,
                exception: _error,
              ),
            ),
          ),
        );
      }
    }
  }

  Widget get buildEmailTextField {
    return TextFormField(
      validator: ValidationBuilder().email().build(),
      textAlignVertical: TextAlignVertical.center,
      controller: _emailController,
      decoration: InputDecoration(
        hintText: "E-Mail",
        prefixIcon: Padding(
          padding: const EdgeInsets.all(
            10,
          ),
          child: Icon(
            MaterialCommunityIcons.email_outline,
            color: ApplicationStyle.primaryColor,
          ),
        ),
      ),
    );
  }

  Widget get buildPasswordTextField {
    return TextFormField(
      validator: ValidationBuilder().minLength(5).build(),
      textAlignVertical: TextAlignVertical.center,
      obscureText: true,
      controller: _passwordController,
      decoration: InputDecoration(
        hintText: "Password",
        prefixIcon: Padding(
          padding: const EdgeInsets.all(
            10,
          ),
          child: Icon(
            MaterialCommunityIcons.key_outline,
            color: ApplicationStyle.primaryColor,
          ),
        ),
      ),
    );
  }

  Widget get buildForgotPassword {
    return CustomTextButton(
      text: "Forgot Password?",
      onPressed: () {},
    );
  }

  Widget buildSocialMediaButton({
    Color color,
    IconData icon,
    VoidCallback onPressed,
  }) {
    return Container(
      width: 50,
      height: 50,
      margin: EdgeInsets.symmetric(
        horizontal: 10,
      ),
      decoration: BoxDecoration(
        color: ApplicationStyle.primaryColor.withOpacity(1),
      ),
      child: Center(
        child: Icon(
          icon,
          color: Colors.white,
        ),
      ),
    );
  }

  Widget get buildSocialMediaRow {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        buildSocialMediaButton(
          color: Color(0xFF3b5998),
          icon: MaterialCommunityIcons.facebook,
        ),
        buildSocialMediaButton(
          color: Color(0xFFDB4437),
          icon: MaterialCommunityIcons.google,
        ),
      ],
    );
  }

  Widget get buildBody {
    return Center(
      child: Padding(
        padding: EdgeInsets.all(
          20,
        ),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomLogo(
                      size: 150,
                    ),
                    Divider(),
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 5,
                      ),
                      child: Text(
                        "Sign In",
                        style: TextStyle(
                          fontFamily: "BreeSerif",
                          fontSize: 20,
                        ),
                      ),
                    ),
                    Divider(),
                    buildEmailTextField,
                    Divider(),
                    buildPasswordTextField,
                    Divider(),
                    Divider(),
                    Align(
                      alignment: Alignment.centerRight,
                      child: buildForgotPassword,
                    ),
                    Divider(),
                    Divider(),
                    CustomButton(
                      maxWidth: true,
                      child: Provider.of<AuthenticationProvider>(context,listen:true).isLoading ? CupertinoActivityIndicator() : Text("Log In"),
                      onPressed: tryLogin,
                    ),
                    Visibility(
                      visible: !_keyboardIsVisible,
                      replacement: Container(),
                      child: Column(
                        children: [
                          Divider(),
                          Divider(),
                          Align(
                            child: Text(
                              "Sign In via Social Media",
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.black26,
                              ),
                            ),
                            alignment: Alignment.center,
                          ),
                          Divider(),
                          buildSocialMediaRow,
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Visibility(
                visible: !_keyboardIsVisible,
                replacement: Container(),
                child: InkWell(
                  onTap: () {
                    ExtendedNavigator.of(context).push(
                      Routes.registrationPage,
                    );
                  },
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: "Dont have an account yet?\n",
                      style: TextStyle(
                        color: Colors.black26,
                        fontSize: 12,
                      ),
                      children: [
                        TextSpan(
                          text: "Register now!",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: ApplicationStyle.primaryColor,
                            fontSize: 15,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: buildBody,
    );
  }
}
