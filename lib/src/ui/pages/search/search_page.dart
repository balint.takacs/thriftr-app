import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:thriftr/src/models/product/product_model.dart';
import 'package:thriftr/src/models/shop/shop_model.dart';
import 'package:thriftr/src/repositories/product_repository.dart';
import 'package:thriftr/src/repositories/search_repository.dart';
import 'package:thriftr/src/resources/extensions.dart';
import 'package:thriftr/src/resources/modal_helper.dart';
import 'package:thriftr/src/resources/style.dart';
import 'package:thriftr/src/ui/widget/bussines/custom_bussines_listtile.dart';
import 'package:thriftr/src/ui/widget/custom_headline.dart';
import 'package:thriftr/src/ui/widget/product/custom_product_grid_tile.dart';

class SearchPage extends StatefulWidget {
  SearchPage({Key key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  List<ProductModel> productsResult;
  List<ShopModel> shopsResult;
  List<String> _recentSearches;

  final TextEditingController _searchController = new TextEditingController();

  bool _searchInClothes = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      SearchRepository.getRecentSearches().then(
        (value) {
          setState(() {
            _recentSearches = value;
          });
        },
      );
    });
  }

  void _switchSearchType() async {
    setState(() {
      _searchInClothes = !_searchInClothes;
    });
    _searchController.clear();
  }

  Future<void> _search(String search) async {
    if (search.length >= 3) {
      if (_searchInClothes) {
        SearchRepository.searchInProducts(search: search).then(
          (value) {
            if (value != null) {
              setState(() {
                productsResult = value;
              });
            }
          },
        );
      } else {
        SearchRepository.searchInShops(search: search).then(
          (value) {
            if (value != null) {
              setState(() {
                shopsResult = value;
              });
            }
          },
        );
      }
    } else {
      setState(() {
        shopsResult = null;
        productsResult = null;
      });
      SearchRepository.getRecentSearches().then(
        (value) {
          setState(() {
            _recentSearches = value;
          });
        },
      );
    }
  }

  Widget get buildSearchBar {
    return TextField(
      controller: _searchController,
      textAlignVertical: TextAlignVertical.center,
      onChanged: _search,
      decoration: InputDecoration(
        prefixIcon: Icon(
          Icons.search,
          color: Colors.black,
        ),
        suffixIcon: IconButton(
          onPressed: () {
            _searchController.clear();
            _search("");

            SearchRepository.getRecentSearches().then(
              (value) {
                setState(() {
                  _recentSearches = value;
                });
              },
            );
          },
          icon: Icon(Icons.close),
        ),
        hintText: "Keresés...",
      ),
    );
  }

  Widget buildRecentSearchTile({String search}) {
    return InkWell(
      onTap: () {
        _search(search);
      },
      child: Row(
        children: [
          Expanded(
            child: Text("$search"),
          ),
          IconButton(
            icon: Icon(
              Icons.close,
              size: 15,
            ),
            onPressed: () {
              SearchRepository.removeRecentSearch(searchQuery: search).then(
                (value) {
                  SearchRepository.getRecentSearches().then(
                    (value) {
                      setState(() {
                        _recentSearches = value;
                      });
                    },
                  );
                },
              );
            },
          ),
        ],
      ),
    );
  }

  Widget get buildRecentSearches {
    if (_recentSearches == null) {
      return Container();
    }
    if (_recentSearches.isEmpty) {
      return Container();
    }
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Divider(),
      CustomHeadline(
        title: "Recent Searches",
      ),
      Divider(),
      Column(
        children: _recentSearches
            .map((e) => buildRecentSearchTile(search: e))
            .toList(),
      ),
    ]);
  }

  Widget get buildTopSearches {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Divider(),
        CustomHeadline(
          title: "Top Searches",
        ),
        Divider(),
        buildRecentSearchTile(),
        buildRecentSearchTile(),
        buildRecentSearchTile(),
      ],
    );
  }

  Widget get buildEmptySearchPage {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 20,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          buildRecentSearches,
          buildTopSearches,
        ],
      ),
    );
  }

  Widget get buildPromoSearchPage {
    return SliverList(
      delegate: SliverChildListDelegate.fixed(
        [
          Divider(),
          buildEmptySearchPage,
        ],
      ),
    );
  }

  Widget get buildResult {
    if (_searchInClothes) {
      if (productsResult == null) {
        return buildPromoSearchPage;
      }

      return SliverPadding(
        padding: EdgeInsets.all(
          20,
        ),
        sliver: SliverGrid(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            childAspectRatio: 0.65,
          ),
          delegate: SliverChildBuilderDelegate(
            (context, count) {
              return CustomProductGridTile(
                productModel: productsResult[count],
              );
            },
            childCount: productsResult.length,
          ),
        ),
      );
    } else {
      if (shopsResult == null) {
        return buildPromoSearchPage;
      }

      return SliverList(
        delegate: SliverChildListDelegate.fixed(
          shopsResult
              .map(
                (e) => CustomBussinesListTile(
                  shopModel: e,
                ),
              )
              .toList(),
        ),
      );
    }
  }

  Widget get buildHeader {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomHeadline(title: "Keresés"),
        Divider(),
        buildSearchBar,
        Divider(),
        Divider(),
        Align(
          alignment: Alignment.center,
          child: Text("Minimum 3 character to search."),
        ),
      ],
    );
  }

  int _selectedSearchFilter = 0;

  Widget get buildSelector {
    return Container(
      width: double.maxFinite,
      child: CupertinoSlidingSegmentedControl(
        groupValue: _selectedSearchFilter,
        onValueChanged: (d) {
          setState(() {
            _selectedSearchFilter = d;

            _searchInClothes = d == 0;
          });
        },
        children: {
          0: Text(
            "Products",
            style: TextStyle(
              fontSize: 15,
              fontFamily: "Montserrat",
            ),
          ),
          1: Text(
            "Sellers",
            style: TextStyle(
              fontSize: 15,
              fontFamily: "Montserrat",
            ),
          )
        },
      ),
    );
  }

  Widget get buildBody {
    return CustomScrollView(
      physics: BouncingScrollPhysics(),
      slivers: [
        SliverAppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          floating: true,
          pinned: true,
          title: buildSelector,
          bottom: PreferredSize(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 20,
              ),
              child: buildSearchBar,
            ),
            preferredSize: Size.fromHeight(48),
          ),
        ),
        buildResult,
      ],
    );
    return Column(
      children: [
        Divider(),
        buildHeader,
        Expanded(
          child: SingleChildScrollView(),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildBody,
      resizeToAvoidBottomPadding: false,
    );
  }
}
