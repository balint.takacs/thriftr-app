import 'dart:developer';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:loadmore/loadmore.dart';
import 'package:provider/provider.dart';
import 'package:thriftr/src/models/product/product_model.dart';
import 'package:thriftr/src/providers/product/product_provider.dart';
import 'package:thriftr/src/providers/shop/shop_provider.dart';
import 'package:thriftr/src/resources/routes/application_routes.dart';
import 'package:thriftr/src/resources/style.dart';
import 'package:thriftr/src/ui/widget/custom_headline.dart';
import 'package:thriftr/src/ui/widget/logo/custom_logo.dart';

class AllProductsPage extends StatefulWidget {
  AllProductsPage({Key key}) : super(key: key);

  @override
  _AllProductsPageState createState() => _AllProductsPageState();
}

class _AllProductsPageState extends State<AllProductsPage> {
  List<ProductModel> allProducts;
  ScrollController _scrollController = new ScrollController();

  int from = 0;
  int to = 30;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(
      (timeStamp) {
        _fetch();
      },
    );
  }

  bool _canFetchs = true;
  void _fetch() async {
    if (!_canFetchs) {
      return;
    }
    _canFetchs = false;
    await Provider.of<ProductProvider>(context, listen: false).fetchAllProducts(
      from: from,
      to: to,
    );

    from += 30;
    from = from - 1;

    to += 30;

    await Future.delayed(Duration(milliseconds: 100));
    _canFetchs = true;
  }

  Widget buildGridProductTile(ProductModel productModel) {
    return InkWell(
      onTap: () {
        ExtendedNavigator.of(context).push(
          Routes.viewProductPage,
          arguments: ViewProductPageArguments(
            productModel: productModel,
          ),
        );
      },
      child: Container(
        child: Image.network(
          productModel.images == null
              ? '-'
              : productModel.images.length <= 0
                  ? '-'
                  : productModel.images.first,
          fit: BoxFit.cover,
          errorBuilder: (d1, d2, d3) {
            return CustomLogo();
          },
        ),
      ),
    );
  }

  Widget get buildBody {
    return NotificationListener<ScrollNotification>(
      onNotification: (notification) {
        if (notification.metrics.pixels >=
            (notification.metrics.maxScrollExtent - 5)) {
          _fetch();
        }
      },
      child: CustomScrollView(
        controller: _scrollController,
        physics: BouncingScrollPhysics(),
        slivers: [
          SliverAppBar(
            automaticallyImplyLeading: false,
            backgroundColor: Colors.white,
            floating: true,
            expandedHeight: 100,
            toolbarHeight: 100,
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    if (Navigator.of(context).canPop()) {
                      Navigator.of(context).pop();
                    }
                  },
                  child: Icon(
                    Icons.close,
                    color: ApplicationStyle.primaryColor,
                  ),
                ),
                Divider(),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 0),
                  child: Row(
                    children: [
                      CustomHeadline(title: "Products"),
                      Spacer(),
                      InkWell(
                        child: Icon(
                          MaterialCommunityIcons.filter_outline,
                          color: ApplicationStyle.primaryColor,
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(),
              ],
            ),
          ),
          SliverGrid(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              crossAxisSpacing: 5,
              mainAxisSpacing: 5,
            ),
            delegate: SliverChildBuilderDelegate(
              (context, count) {
                return buildGridProductTile(
                  allProducts[count],
                );
              },
              childCount: (allProducts == null) ? 0 : allProducts.length,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    allProducts =
        Provider.of<ProductProvider>(context, listen: true).allProducts;
    return Scaffold(
      body: LoadMore(
        child: buildBody,
        onLoadMore: () async {
          await Future.delayed(Duration(seconds: 1));
          print("Load more.");
          return Future.value(true);
        },
      ),
    );
  }
}
