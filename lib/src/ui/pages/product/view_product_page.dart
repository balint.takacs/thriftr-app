import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';
import 'package:thriftr/src/models/product/product_model.dart';
import 'package:thriftr/src/providers/product/product_provider.dart';
import 'package:thriftr/src/resources/style.dart';
import 'package:thriftr/src/ui/widget/button/custom_flat_button.dart';
import 'package:thriftr/src/ui/widget/custom_headline.dart';
import 'package:thriftr/src/ui/widget/custom_price_tag.dart';
import 'package:thriftr/src/ui/widget/custom_stars.dart';

class ViewProductPage extends StatefulWidget {
  ProductModel productModel;
  ViewProductPage({
    Key key,
    this.productModel,
  }) : super(key: key);

  @override
  _ViewProductPageState createState() => _ViewProductPageState();
}

class _ViewProductPageState extends State<ViewProductPage> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(
      (timeStamp) {
        Provider.of<ProductProvider>(context, listen: false)
            .fetchCurrentProduct(
          productId: widget.productModel.id,
        )
            .then((value) {
          if (value != null) {
            setState(() {
              widget.productModel = value;
            });
          }
        });
      },
    );
  }

  Widget buildImage(String link) {
    return Image.network(
      link,
      fit: BoxFit.cover,
      loadingBuilder: (context, child, ImageChunkEvent progress) {
        if (progress == null) return child;
        return Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(
              Colors.black,
            ),
            value: progress.expectedTotalBytes != null
                ? progress.cumulativeBytesLoaded / progress.expectedTotalBytes
                : null,
          ),
        );
      },
    );
  }

  Widget get buildImagesViewer {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.width,
      color: Colors.black12,
      child: PageView(
        physics: BouncingScrollPhysics(),
        children: widget.productModel.images.toList().map(
          (d) {
            return buildImage(d);
          },
        ).toList(),
      ),
    );
  }

  Future<void> like() async {
    if (Provider.of<ProductProvider>(context, listen: false).isLoading) {
      return;
    }
    if (widget.productModel.liked) {
      Exception error =
          await Provider.of<ProductProvider>(context, listen: false)
              .dislikeProduct(productId: widget.productModel.id);

      if (error == null) {
        setState(() {
          widget.productModel = widget.productModel.rebuild(
            (d) => d
              ..liked = false
              ..likeCount = widget.productModel.likeCount - 1,
          );
        });
      }
    } else {
      Exception error =
          await Provider.of<ProductProvider>(context, listen: false)
              .likeProduct(productId: widget.productModel.id);

      if (error == null) {
        setState(() {
          widget.productModel = widget.productModel.rebuild(
            (d) => d
              ..liked = true
              ..likeCount = widget.productModel.likeCount + 1,
          );
        });
      }
    }
  }

  bool _liked = false;
  Widget get buildLikeButton {
    return CustomFlatButton(
      onPressed: () {
        like();
      },
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 5,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(
              (widget.productModel.liked ?? false)
                  ? MaterialCommunityIcons.heart
                  : MaterialCommunityIcons.heart_outline,
              color: ApplicationStyle.primaryColor,
              size: 20,
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              "${widget.productModel.likeCount ?? 0}",
              style: TextStyle(
                color: ApplicationStyle.primaryColor,
              ),
            )
          ],
        ),
      ),
    );
  }

  bool _isFavorite = false;
  Widget get buildBookmarkButton {
    return CustomFlatButton(
      backgroundColor: Colors.white,
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 5,
        ),
        child: Icon(
          MaterialCommunityIcons.bookmark,
          color: ApplicationStyle.primaryColor,
          size: 20,
        ),
      ),
      onPressed: () {},
    );
  }

  Widget get buildMessageButton {
    return Container(
      width: 40,
      height: 40,
      child: FloatingActionButton(
        heroTag: "3",
        backgroundColor: Colors.white,
        child: Icon(
          MaterialCommunityIcons.message_outline,
          size: 20,
          color: Colors.black,
        ),
        onPressed: () {},
      ),
    );
  }

  Widget get buildReactionRow {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          children: <Widget>[
            SizedBox(width: 20),
            buildLikeButton,
            SizedBox(width: 10),
            buildBookmarkButton,
            SizedBox(width: 10),
            // buildMessageButton
          ],
        ),
      ],
    );
  }

  Widget get buildRating {
    return CustomStars(
      size: 10,
      count: 2,
    );
  }

  Widget get buildDetail {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 0,
      ),
      child: Text("${widget.productModel.description}"),
    );
  }

  Widget get buildAboutSeller {
    if (widget.productModel.shop == null) {
      return Text("Loading");
    }
    return Row(
      children: <Widget>[
        CircleAvatar(
          radius: 18,
          backgroundColor: Colors.black26,
          backgroundImage: NetworkImage(
            widget.productModel.shop == null
                ? ""
                : widget.productModel.shop.avatar ?? '-',
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              widget.productModel.shop.name,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.w600),
            ),
            Text(
              "Budapest",
              style: TextStyle(
                color: Colors.black26,
                fontSize: 12,
              ),
            ),
            buildRating
          ],
        )
      ],
    );
  }

  Widget get buildBody {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          buildImagesViewer,
          SizedBox(
            height: 10,
          ),
          buildReactionRow,
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "${widget.productModel.name}",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
                SizedBox(
                  height: 0,
                ),
                buildDetail,
                SizedBox(
                  height: 0,
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text("Méret: "),
                            Text(
                              "${widget.productModel.size}",
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text("Állapot"),
                            Text(
                              "${widget.productModel.condition}",
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text("Brand:"),
                            Text(
                              "${widget.productModel.brand}",
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text("Nem: "),
                            Text(
                              "${widget.productModel.gender}",
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                buildBuyerProtection,
              ],
            ),
          ),
          SizedBox(
            height: 0,
          ),
          GestureDetector(
            onTap: () {},
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 0,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                      left: 20,
                      bottom: 20,
                      top: 10,
                    ),
                    child: CustomHeadline(
                      title: "Eladóról"
                    ),
                  ),
                  Padding(
                    child: buildAboutSeller,
                    padding: EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 0,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                    left: 20,
                    bottom: 20,
                    top: 20,
                  ),
                  child:CustomHeadline(
                      title: "Érdekes termékek"
                  ),
                ),
                buildMoreThings,
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          GestureDetector(
            onTap: () {},
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 70,
              color: ApplicationStyle.primaryColor,
              child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Buy",
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        "( ${widget.productModel.price} ${widget.productModel.currency} )",
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ],
                  ),),
            ),
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  Widget get buildBuyerProtection {
    return Container(
      padding: EdgeInsets.all(
        10,
      ),
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.black,
          width: 2,
        ),
      ),
      child: Row(
        children: <Widget>[
          Icon(
            MaterialCommunityIcons.shield_check_outline,
            size: 30,
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Text("Buyer protection szöveg."),
          ),
        ],
      ),
    );
  }

  Widget buildAllClothesElement() {
    return GestureDetector(
      onTap: () {},
      child: Container(
        height: 150,
        width: 150,
        margin: EdgeInsets.only(right: 10),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            end: Alignment.bottomCenter,
            begin: Alignment.topCenter,
            colors: [
              ApplicationStyle.primaryColor,
              Colors.black,
            ],
          ),
          borderRadius: BorderRadius.circular(
            10,
          ),
        ),
        child: Stack(
          children: <Widget>[
            Positioned.fill(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(
                  10,
                ),
                child: Image.network(
                  "https://via.placeholder.com/100x100",
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
              top: 5,
              right: 5,
              child: Container(
                padding: EdgeInsets.all(
                  5,
                ),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.black38,
                ),
                child: Icon(
                  MaterialCommunityIcons.heart_outline,
                  color: Colors.white,
                  size: 20,
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              right: 0,
              left: 0,
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(
                    10,
                  ),
                  bottomRight: Radius.circular(10),
                ),
                child: Container(
                  height: 70,
                  padding: EdgeInsets.only(
                    top: 20,
                    left: 10,
                  ),
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.black87,
                        Colors.transparent,
                      ],
                      end: Alignment.topCenter,
                      begin: Alignment.bottomCenter,
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "${widget.productModel.name}",
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      CustomPriceTag(price: 1500.0, currency: "HUF"),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget get buildMoreThings {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      physics: BouncingScrollPhysics(),
      child: Row(
        children: [],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        title: widget.productModel.shop == null
            ? Container()
            : GestureDetector(
                onTap: () {},
                child: Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.close,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    CircleAvatar(
                      radius: 18,
                      backgroundColor: Colors.black26,
                      backgroundImage: NetworkImage(
                        widget.productModel.shop == null
                            ? ""
                            : widget.productModel.shop.avatar ?? '-',
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${widget.productModel.shop.name}",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 15,
                              fontWeight: FontWeight.w600),
                        ),
                        Text(
                          "Budapest, Hungary",
                          style: TextStyle(
                            color: Colors.black26,
                            fontSize: 12,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
        actions: <Widget>[
          IconButton(
            icon: Text(
              "FAQ",
              style: TextStyle(
                color: ApplicationStyle.primaryColor,
              ),
            ),
            onPressed: () {},
          ),
          SizedBox(
            width: 10,
          ),
        ],
      ),
      body: buildBody,
    );
  }
}
