import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:thriftr/src/providers/authentication/authentication_provider.dart';
import 'package:thriftr/src/ui/pages/authentication/login_page.dart';
import 'package:thriftr/src/ui/pages/home/home_page.dart';

class RootPage extends StatefulWidget {
  RootPage({Key key}) : super(key: key);

  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  bool isAuthenticated = false;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(
      (timeStamp) {
        Provider.of<AuthenticationProvider>(context, listen: false)
            .checkUserIsAuthenticated();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    isAuthenticated = Provider.of<AuthenticationProvider>(context, listen: true)
        .isAuthenticated;

    if (isAuthenticated) {
      return HomePage();
    }

    return LoginPage();
  }
}
