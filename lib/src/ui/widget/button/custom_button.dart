import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final Widget child;
  final VoidCallback onPressed;
  final bool maxWidth;
  const CustomButton({
    Key key,
    this.maxWidth = false,
    @required this.child,
    @required this.onPressed,
  }) : super(key: key);

  Widget get buildButton {
    return RaisedButton(
      onPressed: onPressed,
      child: child,
    );
  }

  @override
  Widget build(BuildContext context) {
    if(!maxWidth){
      return buildButton;
    }
    return Container(
      width: double.maxFinite,
      child: buildButton,
    );
  }
}
