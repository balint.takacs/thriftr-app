import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomTextButton extends StatelessWidget {
  final TextStyle textStyle;
  final String text;
  final VoidCallback onPressed;

  const CustomTextButton({
    Key key,
    this.textStyle,
    @required this.text,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Text(
        text,
        style: textStyle ?? TextStyle(
          color: Colors.black,
          fontSize: 13,
          fontFamily: "Montserrat",
        ),
      ),
    );
  }
}
