import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:thriftr/src/resources/style.dart'; 

class CustomFlatButton extends StatefulWidget {
  final VoidCallback onPressed;
  final Widget child;
  final Color backgroundColor;
  final EdgeInsets padding;
  CustomFlatButton({
    Key key,
    this.onPressed,
    this.child,
    this.backgroundColor = Colors.transparent,
    this.padding = const EdgeInsets.all(10,),
  }) : super(key: key);

  @override
  _CustomFlatButtonState createState() => _CustomFlatButtonState();
}

class _CustomFlatButtonState extends State<CustomFlatButton> {
  bool tapped = false;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (d) {
        setState(() {
          tapped = true;
        });
      },
      onTapCancel: () {
        setState(() {
          tapped = false;
        });
      },
      onTapUp: (d) {
        setState(() {
          tapped = false;
        });

        widget.onPressed();
      },
      child: AnimatedContainer(
        padding: widget.padding,
        duration: Duration(
          milliseconds: 300,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5,),
          color: widget.backgroundColor,
          border: Border.all(
            color: ApplicationStyle.primaryColor,
          ),
        ),
        child: DefaultTextStyle(
          child: Center(
            child: widget.child,
          ),
          style: TextStyle(
            color: tapped ? Colors.white : ApplicationStyle.primaryColor,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
