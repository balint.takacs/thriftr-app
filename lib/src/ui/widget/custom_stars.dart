import 'package:flutter/material.dart';

class CustomStars extends StatelessWidget {
  final int count;
  final double size;
  final Color color;
  final MainAxisAlignment alignment;
  const CustomStars({
    Key key,
    this.count,
    this.color = Colors.black,
    this.size = 20,
    this.alignment = MainAxisAlignment.center,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment:alignment,
      children: List.generate(
        5,
        (index) {
          if (index < count) {
            return Icon(Icons.star, size: size, color: color,);
          }
          return Icon(Icons.star_border, color: color.withAlpha(100,), size: size,);
        },
      ).toList(),
    );
  }
}
