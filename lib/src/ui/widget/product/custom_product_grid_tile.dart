import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:thriftr/src/models/product/product_model.dart';
import 'package:thriftr/src/resources/routes/application_routes.dart';
import 'package:thriftr/src/resources/style.dart';
import 'package:thriftr/src/ui/pages/product/view_product_page.dart';
import 'package:thriftr/src/ui/widget/custom_price_tag.dart';
import 'package:thriftr/src/ui/widget/logo/custom_logo.dart';

class CustomProductGridTile extends StatelessWidget {
  final ProductModel productModel;
  final bool isPlaceholder;

  const CustomProductGridTile({
    Key key,
    this.productModel,
    this.isPlaceholder = false,
  }) : super(key: key);

  String get image {
    if (productModel == null) {
      return '';
    }
    if (productModel.images == null) {
      return '';
    }

    if (productModel.images.length <= 0) {
      return '';
    }

    return productModel.images.first;
  }

  Widget get buildBody {
    return LayoutBuilder(
      builder: (context, BoxConstraints constraints) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.network(
              image,
              width: constraints.biggest.width,
              height: constraints.biggest.width,
              fit: BoxFit.cover,
            ),
            Divider(),
            Text(
              productModel.name,
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              productModel.description,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: 14,
              ),
            ),
            Divider(),
            CustomPriceTag(
              price: productModel.price,
              currency: productModel.currency,
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: buildBody,
      onTap: () {
        pushNewScreen(
          context,
          customPageRoute: MaterialPageRoute(builder: (d) {
            return ViewProductPage(
              productModel: productModel,
            );
          }),
          screen: ViewProductPage(
            productModel: productModel,
          ),
        );
      },
    );
  }
}
