import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:thriftr/src/models/product/product_model.dart';
import 'package:thriftr/src/resources/routes/application_routes.dart';
import 'package:thriftr/src/resources/style.dart';
import 'package:thriftr/src/ui/widget/custom_price_tag.dart';
import 'package:thriftr/src/ui/widget/logo/custom_logo.dart';

class CustomMinProductTile extends StatelessWidget {
  final bool isFirst;
  final ProductModel productModel;
  const CustomMinProductTile({
    Key key,
    this.productModel,
    this.isFirst = false,
  }) : super(key: key);

  String get image {
    if (productModel == null) {
      return '';
    }
    if (productModel.images == null) {
      return '';
    }

    if (productModel.images.length <= 0) {
      return '';
    }

    return productModel.images.first;
  }

  Widget get buildPicture {
    return Card(
      child: Container(
        width: 70,
        height: 70,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(
            10,
          ),
          child: Image.network(
            image,
            fit: BoxFit.cover,
            errorBuilder: (d1, d2, d3) {
              return Padding(
                padding: EdgeInsets.all(10),
                child: CustomLogo(),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget get buildTitle {
    return Text(
      "${productModel.name}",
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 17,
        fontFamily: 'Montserrat',
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        ExtendedNavigator.of(context).push(
          Routes.viewProductPage,
          arguments: ViewProductPageArguments(
            productModel: productModel,
          )
        );
      },
      child: Container(
        margin: EdgeInsets.only(
          left: isFirst ? ApplicationStyle.paddingValue : 5,
          right: 5,
        ),
        child: Column(
          children: [
            Row(
              children: [
                buildPicture,
                SizedBox(width: 10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    buildTitle,
                    CustomPriceTag(
                      price: productModel.price,
                      currency: "${productModel.currency}",
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
