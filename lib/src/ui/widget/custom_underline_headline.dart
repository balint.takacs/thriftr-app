import 'package:flutter/material.dart';
import 'package:thriftr/src/resources/style.dart';

class CustomUnderlineHeadline extends StatelessWidget {
  final String text;
  const CustomUnderlineHeadline({
    Key key,
    @required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: (text.length * 7.5).toDouble(),
      height: 20,
      child: Stack(
        children: [
          Positioned(
            left: 10,
            width: (text.length * 8).toDouble(),
            bottom: 3,
            child: Container(
              width: double.maxFinite,
              height: 5,
              color: Colors.black12,
            ),
          ),
          Positioned.fill(
            child: Text(
              text,
              style: TextStyle(
                color: ApplicationStyle.primaryColor,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
