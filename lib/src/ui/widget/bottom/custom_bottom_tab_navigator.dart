import 'dart:ui';

import 'package:flutter/material.dart';

class CustomBottomTabNavigator extends StatefulWidget {
  List<CustomTabNavigatorItem> children;
  final ValueChanged<int> onPageChanged;

  CustomBottomTabNavigator({
    Key key,
    this.children,
    this.onPageChanged,
  }) : super(key: key);

  @override
  _CustomBottomTabNavigatorState createState() =>
      _CustomBottomTabNavigatorState();
}

class _CustomBottomTabNavigatorState extends State<CustomBottomTabNavigator> {
  int _currentPageCount = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.children == null) return Container();

    return Container(
      width: MediaQuery.of(context).size.width,
      height: kBottomNavigationBarHeight + 10,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          top: BorderSide(
            color: Colors.black.withOpacity(0.05),
          ),
        ),
      ),
      child: Center(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: widget.children.map((item) {
              return Expanded(
                child: item,
              );
            }).toList(),
          ),
        ),
    );
  }
}

class CustomTabNavigatorItem extends StatefulWidget {
  final String title;
  final IconData icon;
  final IconData fullIcon;
  bool selected = false;
  final VoidCallback onPressed;
  final bool mainAction;
  
  CustomTabNavigatorItem({
    this.title,
    this.icon,
    this.fullIcon,
    this.onPressed,
    this.selected,
    this.mainAction = false,
  });

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CustomTabNavigatorItemState();
  }
}

class _CustomTabNavigatorItemState extends State<CustomTabNavigatorItem> {
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: widget.onPressed,
      splashColor: Colors.black26,
      padding: EdgeInsets.all(0),
      child: LayoutBuilder(
        builder: (c, b) {
          if (widget.mainAction) {
            return Container(
              width: b.maxWidth,
              height: b.maxHeight,
              margin: EdgeInsets.all(10),
              color: Colors.black,
              child: Icon(
                widget.icon,
                color: Colors.white,
              ),
            );
          }
          return AnimatedContainer(
            width: b.maxWidth,
            duration: Duration(
              milliseconds: 300,
            ),
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(
                  color: widget.selected ? Colors.black : Colors.transparent,
                  width: 4
                ),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    !widget.selected
                        ? Container()
                        : Positioned(
                            left: 20,
                            bottom: -2,
                            child: Center(
                              child: Icon(
                                widget.fullIcon,
                                color: Color(0xFF2E53A1).withAlpha(0),
                                size: 25,
                              ),
                            ),
                          ),
                    Center(
                      child: Icon(
                        widget.icon,
                        color:widget.selected ? Colors.black : Colors.black12,
                        size: 25,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
