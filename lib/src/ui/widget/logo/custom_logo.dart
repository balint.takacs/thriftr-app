import 'package:flutter/material.dart';
import 'package:thriftr/src/resources/style.dart';

class CustomLogo extends StatelessWidget {
  final double size;
  final bool isDark;
  const CustomLogo({Key key, this.size = 200, this.isDark = true}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.asset(
        "assets/brand/logo/logo_black.png",
        width: size,
        fit: BoxFit.fitWidth,
        color: isDark ? ApplicationStyle.primaryColor : Colors.white,
    );
  }
}