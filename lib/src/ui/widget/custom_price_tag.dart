import 'package:flutter/material.dart';
import 'package:thriftr/src/resources/style.dart';

class CustomPriceTag extends StatelessWidget {
  final double price;
  final String currency;
  const CustomPriceTag({
    Key key,
    @required this.price,
    @required this.currency,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          color: ApplicationStyle.primaryColor,
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
          child: Text(
            "${price}",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            fontSize: 12,
            ),
          ),
        ),
        SizedBox(width: 5),
        Text(
          "$currency",
          style: TextStyle(
            color: ApplicationStyle.primaryColor,
            fontSize: 10,
          ),
        ),
      ],
    );
  }
}
