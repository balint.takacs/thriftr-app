import 'package:flutter/material.dart';
import 'package:thriftr/src/models/product/brand/product_brand_model.dart';
import 'package:thriftr/src/resources/style.dart';

class CustomMinBrandTile extends StatefulWidget {
  final ProductBrandModel productBrandModel;
  CustomMinBrandTile({Key key, this.productBrandModel,}) : super(key: key);

  @override
  _CustomMinBrandTileState createState() => _CustomMinBrandTileState();
}

class _CustomMinBrandTileState extends State<CustomMinBrandTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: ApplicationStyle.primaryColor.withOpacity(0.1),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height:5),
          Image.network(
            widget.productBrandModel.image,
            height: 50, 
          ), 
          SizedBox(height:5),
        ],
      ),
    );
  }
}
