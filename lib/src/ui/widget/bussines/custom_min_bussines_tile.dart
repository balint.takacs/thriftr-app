import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:thriftr/src/models/shop/shop_model.dart';
import 'package:thriftr/src/resources/routes/application_routes.dart';
import 'package:thriftr/src/ui/widget/custom_stars.dart';

class CustomMinBussinesTile extends StatelessWidget {
  final ShopModel shopModel;
  const CustomMinBussinesTile({
    Key key,
    this.shopModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        ExtendedNavigator.of(context).push(
          Routes.viewBussinesPage,
          arguments: ViewBussinesPageArguments(
            shopModel: shopModel,
          ),
        );
      },
      child: Card(
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Stack(
            children: [
              Positioned.fill(
                child: Image.network(
                  "${shopModel.avatar}",
                  fit: BoxFit.cover,
                ),
              ),
              Positioned.fill(
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.black45,
                        Colors.transparent,
                      ],
                      end: Alignment.topCenter,
                      begin: Alignment.bottomCenter,
                    ),
                  ),
                ),
              ),
              Positioned(
                bottom: 10,
                left: 10,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      "${shopModel.name}",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    CustomStars(
                      count: 2,
                      color: Colors.white,
                      size: 10,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
