import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:thriftr/src/models/shop/shop_model.dart';
import 'package:thriftr/src/resources/routes/application_routes.dart';
import 'package:thriftr/src/resources/style.dart';
import 'package:thriftr/src/ui/pages/bussines/view_bussines_page.dart';
import 'package:thriftr/src/ui/widget/custom_stars.dart';

class CustomBussinesListTile extends StatelessWidget {
  final ShopModel shopModel;
  const CustomBussinesListTile({
    Key key,
    this.shopModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        pushNewScreen(
          context,
          screen: ViewBussinesPage(
            shopModel: shopModel,
          ),
        );
      },
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.all(
                ApplicationStyle.paddingValue,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        border: Border.all(
                      color: Colors.black,
                    )),
                    child: Image.network(
                      shopModel.avatar,
                      height: 70,
                      width: 70,
                    ),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${shopModel.name}",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        CustomStars(
                          count: 3,
                          color: ApplicationStyle.primaryColor,
                          size: 10,
                          alignment: MainAxisAlignment.start,
                        ),
                        Divider(),
                        Text(
                          "Budapest, Hungary",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w200,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 1,
              width: double.maxFinite,
              color: Colors.black12,
            ),
          ],
        ),
      ),
    );
  }
}
