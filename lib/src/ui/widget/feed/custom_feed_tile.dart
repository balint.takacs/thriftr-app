import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:thriftr/src/models/product/product_model.dart';
import 'package:thriftr/src/ui/widget/button/custom_flat_button.dart';

class CustomFeedTile extends StatefulWidget {
  final ProductModel productModel;

  CustomFeedTile({
    Key key,
    this.productModel,
  }) : super(key: key);

  @override
  _CustomFeedTileState createState() {
    return _CustomFeedTileState();
  }
}

class _CustomFeedTileState extends State<CustomFeedTile> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget get buildFeedHeader {
    return Padding(
      padding: EdgeInsets.all(
        10,
      ),
      child: Row(
        children: [
          CircleAvatar(
            backgroundImage: NetworkImage(
              "https://via.placeholder.com/100x100",
            ),
            radius: 20,
          ),
          SizedBox(
            width: 10,
          ),
          Text(
            "shopname",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          Spacer(),
        ],
      ),
    );
  }

  Widget get buildImageView {
    return Container(
      height: MediaQuery.of(context).size.width,
      width: MediaQuery.of(context).size.width,
      child: PageView(
        children: widget.productModel.images
            .map(
              (d) => Image.network(
                d,
                fit: BoxFit.cover,
              ),
            )
            .toList(),
      ),
    );
  }

  Widget get buildReactionsRow {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal:10,
      ),
      child: Row(
        children: [
          InkWell(
            child: Padding(
              padding: EdgeInsets.all(
                5,
              ),
              child: Icon(
                Icons.favorite,
                color: Colors.redAccent,
                size: 30,
              ),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Padding(
            padding: EdgeInsets.all(
              5,
            ),
            child: Icon(
              Icons.bookmark_outline,
              color: Colors.black,
              size: 30,
            ),
          ),
        ],
      ),
    );
  }

  Widget get buildDescription {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal:10,
      ),
      child: RichText(
        text: TextSpan(
          text: widget.productModel.name + "\n",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.black,
            fontSize: 18,
          ),
          children: [
            TextSpan(
              text: widget.productModel.description,
              style: TextStyle(
                fontWeight: FontWeight.normal,
                color: Colors.black,
                fontSize: 15,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildCommentTile() {
    return RichText(

      maxLines: 2,
      overflow: TextOverflow.ellipsis,
      textWidthBasis: TextWidthBasis.longestLine,
      text: TextSpan(
        text: "name ",
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
        ),
        children: [
          TextSpan(
            text:
                "commedsadsantcommedsadsantcomdsadsadsadsadaadamedsadsantcommedsadsantcommedsadsantcommedsadsant",
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.normal,

            ),
          ),
        ],
      ),
    );
  }

  Widget get buildComments {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal:10,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            child: Text(
              "View comments",
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Divider(),
              buildCommentTile(),
              SizedBox(height:5),
              buildCommentTile(),
              SizedBox(height:5),
              buildCommentTile(),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          buildFeedHeader,
          buildImageView,
          buildReactionsRow,
          buildDescription,
          buildComments
        ],
      ),
    );
  }
}
