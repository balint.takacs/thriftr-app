import 'package:flutter/material.dart';
import 'package:thriftr/src/resources/style.dart';

class CustomHeadline extends StatelessWidget {
  final String title;
  const CustomHeadline({
    Key key,
    @required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: TextStyle(
        color: ApplicationStyle.primaryColor,
        fontFamily: "Montserrat",
        fontWeight: FontWeight.bold,
        fontSize: 20,
      ),
    );
  }
}
