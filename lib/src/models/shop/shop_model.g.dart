// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shop_model.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ShopModel> _$shopModelSerializer = new _$ShopModelSerializer();

class _$ShopModelSerializer implements StructuredSerializer<ShopModel> {
  @override
  final Iterable<Type> types = const [ShopModel, _$ShopModel];
  @override
  final String wireName = 'ShopModel';

  @override
  Iterable<Object> serialize(Serializers serializers, ShopModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(int)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.description != null) {
      result
        ..add('description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    if (object.avatar != null) {
      result
        ..add('avatar')
        ..add(serializers.serialize(object.avatar,
            specifiedType: const FullType(String)));
    }
    if (object.cover != null) {
      result
        ..add('cover')
        ..add(serializers.serialize(object.cover,
            specifiedType: const FullType(String)));
    }
    if (object.created != null) {
      result
        ..add('created')
        ..add(serializers.serialize(object.created,
            specifiedType: const FullType(String)));
    }
    if (object.follow != null) {
      result
        ..add('follow')
        ..add(serializers.serialize(object.follow,
            specifiedType: const FullType(bool)));
    }
    if (object.followers != null) {
      result
        ..add('followers')
        ..add(serializers.serialize(object.followers,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  ShopModel deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ShopModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'avatar':
          result.avatar = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'cover':
          result.cover = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'created':
          result.created = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'follow':
          result.follow = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'followers':
          result.followers = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$ShopModel extends ShopModel {
  @override
  final int id;
  @override
  final String name;
  @override
  final String description;
  @override
  final String avatar;
  @override
  final String cover;
  @override
  final String created;
  @override
  final bool follow;
  @override
  final int followers;

  factory _$ShopModel([void Function(ShopModelBuilder) updates]) =>
      (new ShopModelBuilder()..update(updates)).build();

  _$ShopModel._(
      {this.id,
      this.name,
      this.description,
      this.avatar,
      this.cover,
      this.created,
      this.follow,
      this.followers})
      : super._();

  @override
  ShopModel rebuild(void Function(ShopModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ShopModelBuilder toBuilder() => new ShopModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ShopModel &&
        id == other.id &&
        name == other.name &&
        description == other.description &&
        avatar == other.avatar &&
        cover == other.cover &&
        created == other.created &&
        follow == other.follow &&
        followers == other.followers;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, id.hashCode), name.hashCode),
                            description.hashCode),
                        avatar.hashCode),
                    cover.hashCode),
                created.hashCode),
            follow.hashCode),
        followers.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ShopModel')
          ..add('id', id)
          ..add('name', name)
          ..add('description', description)
          ..add('avatar', avatar)
          ..add('cover', cover)
          ..add('created', created)
          ..add('follow', follow)
          ..add('followers', followers))
        .toString();
  }
}

class ShopModelBuilder implements Builder<ShopModel, ShopModelBuilder> {
  _$ShopModel _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  String _avatar;
  String get avatar => _$this._avatar;
  set avatar(String avatar) => _$this._avatar = avatar;

  String _cover;
  String get cover => _$this._cover;
  set cover(String cover) => _$this._cover = cover;

  String _created;
  String get created => _$this._created;
  set created(String created) => _$this._created = created;

  bool _follow;
  bool get follow => _$this._follow;
  set follow(bool follow) => _$this._follow = follow;

  int _followers;
  int get followers => _$this._followers;
  set followers(int followers) => _$this._followers = followers;

  ShopModelBuilder();

  ShopModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _description = _$v.description;
      _avatar = _$v.avatar;
      _cover = _$v.cover;
      _created = _$v.created;
      _follow = _$v.follow;
      _followers = _$v.followers;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ShopModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ShopModel;
  }

  @override
  void update(void Function(ShopModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ShopModel build() {
    final _$result = _$v ??
        new _$ShopModel._(
            id: id,
            name: name,
            description: description,
            avatar: avatar,
            cover: cover,
            created: created,
            follow: follow,
            followers: followers);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
