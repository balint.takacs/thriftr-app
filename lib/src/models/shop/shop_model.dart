


import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'shop_model.g.dart';

/// Ez a Model tárolja a felhasználó adatait.
abstract class ShopModel implements Built<ShopModel, ShopModelBuilder> {

  /// Erre minden egyes modelnél szükség van, a megvalósulását a Generátor biztosítja.
  static Serializer<ShopModel> get serializer => _$shopModelSerializer;

  @nullable
  @BuiltValueField(wireName:"id")
  int get id;

  @nullable
  @BuiltValueField(wireName:"name")
  String get name;

  @nullable
  @BuiltValueField(wireName:"description")
  String get description;

  @nullable
  @BuiltValueField(wireName:"avatar")
  String get avatar;

  @nullable
  @BuiltValueField(wireName:"cover")
  String get cover;

  @nullable
  @BuiltValueField(wireName:"created")
  String get created;

  @nullable
  bool get follow;

  @nullable
  int get followers;

  factory ShopModel([void Function(ShopModelBuilder) updates]) =
      _$ShopModel;
  ShopModel._();
}