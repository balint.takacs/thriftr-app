


import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:thriftr/src/models/shop/shop_model.dart';

part 'product_brand_model.g.dart';

/// Ez a Model tárolja a felhasználó adatait.
abstract class ProductBrandModel implements Built<ProductBrandModel, ProductBrandModelBuilder> {

  /// Erre minden egyes modelnél szükség van, a megvalósulását a Generátor biztosítja.
  static Serializer<ProductBrandModel> get serializer => _$productBrandModelSerializer;

  @nullable
  @BuiltValueField(wireName:"id")
  int get id;

  @nullable
  @BuiltValueField(wireName:"name")
  String get name;

  @nullable
  @BuiltValueField(wireName:"image")
  String get image;


  factory ProductBrandModel([void Function(ProductBrandModelBuilder) updates]) =
      _$ProductBrandModel;
  ProductBrandModel._();
}