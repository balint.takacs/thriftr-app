// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_brand_model.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProductBrandModel> _$productBrandModelSerializer =
    new _$ProductBrandModelSerializer();

class _$ProductBrandModelSerializer
    implements StructuredSerializer<ProductBrandModel> {
  @override
  final Iterable<Type> types = const [ProductBrandModel, _$ProductBrandModel];
  @override
  final String wireName = 'ProductBrandModel';

  @override
  Iterable<Object> serialize(Serializers serializers, ProductBrandModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(int)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.image != null) {
      result
        ..add('image')
        ..add(serializers.serialize(object.image,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  ProductBrandModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProductBrandModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'image':
          result.image = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ProductBrandModel extends ProductBrandModel {
  @override
  final int id;
  @override
  final String name;
  @override
  final String image;

  factory _$ProductBrandModel(
          [void Function(ProductBrandModelBuilder) updates]) =>
      (new ProductBrandModelBuilder()..update(updates)).build();

  _$ProductBrandModel._({this.id, this.name, this.image}) : super._();

  @override
  ProductBrandModel rebuild(void Function(ProductBrandModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProductBrandModelBuilder toBuilder() =>
      new ProductBrandModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProductBrandModel &&
        id == other.id &&
        name == other.name &&
        image == other.image;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, id.hashCode), name.hashCode), image.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProductBrandModel')
          ..add('id', id)
          ..add('name', name)
          ..add('image', image))
        .toString();
  }
}

class ProductBrandModelBuilder
    implements Builder<ProductBrandModel, ProductBrandModelBuilder> {
  _$ProductBrandModel _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _image;
  String get image => _$this._image;
  set image(String image) => _$this._image = image;

  ProductBrandModelBuilder();

  ProductBrandModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _image = _$v.image;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProductBrandModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProductBrandModel;
  }

  @override
  void update(void Function(ProductBrandModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProductBrandModel build() {
    final _$result =
        _$v ?? new _$ProductBrandModel._(id: id, name: name, image: image);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
