


import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:thriftr/src/models/shop/shop_model.dart';

part 'product_model.g.dart';

/// Ez a Model tárolja a felhasználó adatait.
abstract class ProductModel implements Built<ProductModel, ProductModelBuilder> {

  /// Erre minden egyes modelnél szükség van, a megvalósulását a Generátor biztosítja.
  static Serializer<ProductModel> get serializer => _$productModelSerializer;

  @nullable
  @BuiltValueField(wireName:"id")
  int get id;

  @nullable
  @BuiltValueField(wireName:"name")
  String get name;

  @nullable
  @BuiltValueField(wireName:"description")
  String get description;

  @nullable
  @BuiltValueField(wireName:"brand")
  String get brand;

  @nullable
  @BuiltValueField(wireName:"gender")
  String get gender;

  @nullable
  @BuiltValueField(wireName:"condition")
  String get condition;
  
  @nullable
  @BuiltValueField(wireName:"size")
  String get size;
  
  @nullable
  @BuiltValueField(wireName:"currency")
  String get currency;
  
  @nullable
  @BuiltValueField(wireName:"price")
  double get price;
  
  @nullable
  @BuiltValueField(wireName:"liked")
  bool get liked;
  
  @nullable
  @BuiltValueField(wireName:"likeCount")
  int get likeCount;

  @nullable
  @BuiltValueField(wireName:"images")
  BuiltList<String> get images;

  @nullable
  ShopModel get shop;

  factory ProductModel([void Function(ProductModelBuilder) updates]) =
      _$ProductModel;
  ProductModel._();
}