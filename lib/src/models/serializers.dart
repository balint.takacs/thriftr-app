import 'package:built_collection/built_collection.dart';
import 'package:built_value/iso_8601_date_time_serializer.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:thriftr/src/models/product/brand/product_brand_model.dart';
import 'package:thriftr/src/models/product/product_model.dart';
import 'package:thriftr/src/models/shop/shop_model.dart'; 
import 'package:thriftr/src/models/user/user_model.dart';


part 'serializers.g.dart';

@SerializersFor(const [
  UserModel, 
  ShopModel,
  ProductModel,
  ProductBrandModel,
])
final Serializers serializers = (_$serializers.toBuilder()
      ..addPlugin(StandardJsonPlugin())
      ..add(Iso8601DateTimeSerializer()))
    .build();
