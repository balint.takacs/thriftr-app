


import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'user_model.g.dart';

/// Ez a Model tárolja a felhasználó adatait.
abstract class UserModel implements Built<UserModel, UserModelBuilder> {

  /// Erre minden egyes modelnél szükség van, a megvalósulását a Generátor biztosítja.
  static Serializer<UserModel> get serializer => _$userModelSerializer;

  @nullable
  @BuiltValueField(wireName:"id")
  int get id;

  @nullable
  @BuiltValueField(wireName:"email")
  String get email;

  @nullable
  @BuiltValueField(wireName:"firstName")
  String get firstName;

  @nullable
  @BuiltValueField(wireName:"lastName")
  String get lastName; 
  
  @nullable
  bool get hasShop; 

  factory UserModel([void Function(UserModelBuilder) updates]) =
      _$UserModel;
  UserModel._();
}