import 'package:flutter/material.dart';
import 'package:thriftr/src/models/user/user_model.dart';
import 'package:thriftr/src/repositories/user_repository.dart';
import 'package:thriftr/src/resources/interfaces/loading_interface.dart';

class UserProvider extends LoadingInterface {
  UserModel currentUser;

  Future<Exception> updateProfile({@required UserModel newUser}) async {
    try {
      isLoading = true;
      await UserRepository.updateProfile(
        newUser: newUser,
      );
      await fetchUser();
      isLoading = false;
      return null;
    } catch (e) {
      return e;
    }
  }

  Future<UserModel> fetchUser() async {
    isLoading = true;
    currentUser = await UserRepository.getCurrentUser();
    isLoading = false;
    return currentUser;
  }
}
