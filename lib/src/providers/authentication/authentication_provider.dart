import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:thriftr/src/repositories/user_repository.dart';
import 'package:thriftr/src/resources/interfaces/loading_interface.dart';

enum SignInType {
  NORMAL,
  FACEBOOK,
  APPLE,
}

class AuthenticationProvider extends LoadingInterface {
  bool isAuthenticated = true;

  Future<Exception> signIn(
    SignInType type, {
    String email,
    String password,
  }) async {
    try {
      isLoading = true;

      if (type == SignInType.NORMAL) {
        String token = await UserRepository.signIn(
          email: email,
          password: password,
        );

        await UserRepository.persistToken("$token");
      }

      isLoading = false;
      checkUserIsAuthenticated();
      return null;
    } catch (error) {
      Logger.root.log(Level.OFF, "[Auth]: " + error.toString());

      isLoading = false;
      return error;
    }
  }

  Future<Exception> signUp({
    String email,
    String password,
    String firstName,
    String lastName, 
  }) async {
    try {
      isLoading = true;

      await UserRepository.signUp(
        email: email,
        password: password,
        firstName: firstName,
        lastName: lastName, 
      );
      isLoading = false;
      return null;
    } catch (error) {
      Logger.root.log(Level.OFF, "[Auth]: " + error.toString());
      isLoading = false;
      return error;
    }
  }

  Future<void> checkUserIsAuthenticated() async {
    isLoading = true;
    isAuthenticated = await UserRepository.userIsAuthenticated();
    isLoading = false;
    notifyListeners();
  }
}
