import 'package:flutter/material.dart';
import 'package:thriftr/src/models/product/product_model.dart';
import 'package:thriftr/src/models/shop/shop_model.dart';
import 'package:thriftr/src/repositories/shop_repository.dart';
import 'package:thriftr/src/resources/interfaces/loading_interface.dart';

class ShopProvider extends LoadingInterface {
  ShopModel currentShop;

  List<ProductModel> shopProducts;
  List<ShopModel> mostVisitedShops;
  List<ShopModel> allShops;

  Future<Exception> followShop({@required int shopId}) async {
    try {
      isLoading = true;
      await ShopRepository.followShop(
        shopId: shopId
      );
      isLoading = false;
      return null;
    } catch (e) {
      isLoading = false;
      return e;
    }
  }
  
  Future<Exception> unFollowShop({@required int shopId}) async {
    try {
      isLoading = true;
      await ShopRepository.unFollowShop(
        shopId: shopId
      );
      isLoading = false;
      return null;
    } catch (e) {
      isLoading = false;
      return e;
    }
  }
  
  Future<ShopModel> fetchShopDetails({@required int shopId}) async {
    try {
      isLoading = true;
      currentShop = await ShopRepository.getShopDetails(
        shopId: shopId
      );
      isLoading = false;
      return currentShop;
    } catch (e) {
      isLoading = false;
      return null;
    }
  }

  Future<List<ShopModel>> fetchMostVisitedShops() async {
    try {
      isLoading = true;
      mostVisitedShops = await ShopRepository.getMostVisitedShops();
      isLoading = false;
      return mostVisitedShops;
    } catch (e) {
      isLoading = false;
      return mostVisitedShops;
    }
  }

  Future<List<ProductModel>> fetchShopProducts({@required int shopId}) async {
    try {
      shopProducts = null;
      isLoading = true;
      shopProducts = await ShopRepository.getShopProducts(shopid: shopId);
      isLoading = false;
      return shopProducts;
    } catch (e) {
      isLoading = false;
      return shopProducts;
    }
  }

  Future<List<ShopModel>> fetchAllShops() async {
    try {
      isLoading = true;
      allShops = await ShopRepository.getAllShops();
      isLoading = false;
      return allShops;
    } catch (e) {
      isLoading = false;
      return allShops;
    }
  }
}
