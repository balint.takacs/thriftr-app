import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:thriftr/src/models/product/brand/product_brand_model.dart';
import 'package:thriftr/src/models/product/product_model.dart';
import 'package:thriftr/src/repositories/product_repository.dart';
import 'package:thriftr/src/resources/interfaces/loading_interface.dart';

class ProductDatas {
  List<ProductBrandModel> brands;
  List<dynamic> sizes;
  List<dynamic> conditions;
  List<dynamic> genders;
  List<dynamic> currencies;

  ProductDatas({
    this.brands,
    this.sizes,
    this.conditions,
    this.genders,
    this.currencies,
  });
}

class ProductProvider extends LoadingInterface {
  ProductDatas productDatas;

  List<ProductModel> mostPopularProducts;
  List<ProductModel> allProducts;
  ProductModel currentProduct;

  Future<void> fetchProductDatas() async {
    try {
      isLoading = true;
      productDatas = new ProductDatas(
        brands: await ProductRepository.getBrands(),
        sizes: await ProductRepository.getSizes(),
        conditions: await ProductRepository.getConditions(),
        genders: await ProductRepository.getGenders(),
        currencies: await ProductRepository.getCurrencies(),
      );
      isLoading = false;
    } catch (e) {
      isLoading = false;
    }
  }

  Future<List<ProductModel>> fetchMostPopularProducts() async {
    try {
      isLoading = true;
      mostPopularProducts = await ProductRepository.getMostPopularProducts();
      isLoading = false;
      return mostPopularProducts;
    } catch (e) {
      isLoading = false;

      return mostPopularProducts;
    }
  }

  Future<List<ProductModel>> fetchAllProducts(
      {int from = 0, int to = 30}) async {
    try {
      if (from == 0) {
        allProducts?.clear();
        allProducts = null;
      }
      isLoading = true;
      if (allProducts == null) {
        allProducts = new List();
      }
      allProducts.addAll(
        await ProductRepository.getAllProducts(
          from: from,
          to: to,
        ),
      );
      isLoading = false;
      return allProducts;
    } catch (e) {
      isLoading = false;

      Logger.root.log(Level.ALL, "[Error] $e");

      return allProducts;
    }
  }

  Future<ProductModel> fetchCurrentProduct({@required int productId}) async {
    try {
      isLoading = true;
      currentProduct = await ProductRepository.getProductDetails(
        productId: productId,
      );
      isLoading = false;
      return currentProduct;
    } catch (e) {
      isLoading = false;

      return currentProduct;
    }
  }

  Future<Exception> likeProduct({@required int productId}) async {
    try {
      isLoading = true;
      await ProductRepository.likeProduct(
        productId: productId,
      );
      isLoading = false;
      return null;
    } catch (e) {
      isLoading = false;

      return e;
    }
  }

  Future<Exception> dislikeProduct({@required int productId}) async {
    try {
      isLoading = true;
      await ProductRepository.dislikeProduct(
        productId: productId,
      );
      isLoading = false;
      return null;
    } catch (e) {
      isLoading = false;

      return e;
    }
  }
}
