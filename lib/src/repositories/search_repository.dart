import 'dart:developer';

import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:thriftr/src/models/product/product_model.dart';
import 'package:thriftr/src/models/serializers.dart';
import 'package:thriftr/src/models/shop/shop_model.dart';
import 'package:thriftr/src/resources/exception_helper.dart';
import 'package:thriftr/src/resources/key_helper.dart';
import 'package:thriftr/src/services/api/product/product_service.dart';
import 'package:thriftr/src/services/api/shop/shop_service.dart';

class SearchRepository {

  static Future<List<String>> getRecentSearches() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    return preferences.getStringList(KeyHelper.RECENT_SEARCHES_KEY) ?? [];
  }

  static Future<void> removeRecentSearch({String searchQuery}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    List<String> recentSearches = await getRecentSearches();

    if (searchQuery.length >= 5) {
      recentSearches.remove( searchQuery);
      preferences.setStringList(KeyHelper.RECENT_SEARCHES_KEY, recentSearches);
    }
  }

  static Future<void> saveRecentSearches({String searchQuery}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    List<String> recentSearches = await getRecentSearches();

    if (searchQuery.length >= 5) {
      recentSearches.insert(0, searchQuery);

      if (recentSearches.length >= 5) {
        recentSearches.removeLast();
      }
      preferences.setStringList(KeyHelper.RECENT_SEARCHES_KEY, recentSearches);
    }
  }

  static DateTime _lastSearchDate;

  static Future<List<ProductModel>> searchInProducts(
      {@required String search}) async {
    Response response =
        await ProductService.create().searchInProducts(search: search);

    if (response.body["status"] != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }

    if (_lastSearchDate != null) {
      Future.delayed(Duration(seconds: 3)).then(
        (value) {
          if(DateTime.now().difference(_lastSearchDate).inSeconds > 2){
            saveRecentSearches(searchQuery: search);
            log("SAVE SEARCH!!!");
          }
        },
      );
    }
    _lastSearchDate = DateTime.now();
    List<dynamic> data = response.body["data"];

    List<ProductModel> returner = new List();

    for (int x = 0; x < data.length; x++) {
      ProductModel model = serializers.deserializeWith(
        ProductModel.serializer,
        data[x],
      );
      returner.add(model);
    }

    return returner;
  }

  static Future<List<ShopModel>> searchInShops(
      {@required String search}) async {
    Response response =
        await ShopService.create().searchInShops(search: search);

    if (response.body["status"] != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }

    saveRecentSearches(searchQuery: search);

    List<dynamic> data = response.body["data"];

    List<ShopModel> returner = new List();

    for (int x = 0; x < data.length; x++) {
      ShopModel model = serializers.deserializeWith(
        ShopModel.serializer,
        data[x],
      );
      returner.add(model);
    }

    return returner;
  }
}
