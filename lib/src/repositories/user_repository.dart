import 'dart:convert';
import 'dart:developer';

import 'package:chopper/chopper.dart';

import 'package:logging/logging.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:thriftr/src/exceptions/unkown_error_exception.dart';
import 'package:thriftr/src/exceptions/user/user_not_found_exception.dart';
import 'package:thriftr/src/exceptions/user/user_not_registered.dart';
import 'package:thriftr/src/models/serializers.dart';
import 'package:thriftr/src/models/user/user_model.dart';
import 'package:thriftr/src/resources/exception_helper.dart';
import 'package:thriftr/src/resources/key_helper.dart';
import 'package:thriftr/src/resources/route_helper.dart';
import 'package:thriftr/src/services/api/authentication/authentication_service.dart';
import 'package:thriftr/src/services/api/user/user_service.dart';

import 'package:http/http.dart' as http;

class UserRepository {
  static Future<UserModel> getCurrentUser() async {
    String email = await userToken;
    print("Email: $email");
    Response response = await UserService.create().getCurrentUser();

    if (response.body["status"] != "success") {
      throw UserNotFoundException();
    }

    UserModel userModel = serializers.deserializeWith(
      UserModel.serializer,
      response.body["data"],
    );

    return userModel;
  }

  static Future<void> updateProfile({UserModel newUser}) async {}

  static Future<void> forgotPassword({String email}) async {
    Response response =
        await AuthenticationService.create().forgotPassword(email: email);

    if (response.body["status"] != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }
  }

  static Future<String> signIn({
    String email,
    String password,
  }) async {
    Response response = await AuthenticationService.create().login(
      email: email,
      password: password,
    );

    if (response.body["status"] != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }

    return response.body["data"]["token"];
  }

  static Future<String> signUp({
    String email,
    String password,
    String firstName,
    String lastName,
  }) async {
    Response response = await AuthenticationService.create().registration(
      email: email,
      password: password,
      firstName: firstName,
      lastName: lastName,
    );
    if (response.bodyString.isEmpty) {
      throw UnkownError();
    }
    if (response.statusCode != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }
  }

  static Future<bool> userIsAuthenticated() async {
    String _token = await userToken;

    if (_token == null) {
      return false;
    }

    return _token.isNotEmpty && _token.length > 3;
  }

  static Future<void> logout() async {
    await clearToken();
  }

  static Future<String> get userToken async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(KeyHelper.TOKEN_KEY);
  }

  static Future<void> persistToken(String token) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setString(KeyHelper.TOKEN_KEY, token);
  }

  static Future<void> clearToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    if (sharedPreferences.containsKey(KeyHelper.TOKEN_KEY)) {
      sharedPreferences.remove(KeyHelper.TOKEN_KEY);
    }
  }
}
