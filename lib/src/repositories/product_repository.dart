import 'package:chopper/chopper.dart';
import 'package:thriftr/src/models/product/brand/product_brand_model.dart';
import 'package:thriftr/src/models/product/product_model.dart';
import 'package:thriftr/src/models/serializers.dart';
import 'package:thriftr/src/resources/exception_helper.dart';
import 'package:thriftr/src/services/api/product/product_service.dart';

class ProductRepository {
  static Future<List<ProductModel>> getMostPopularProducts() async {
    Response response = await ProductService.create().getMostPopularProducts();

    if (response.body["status"] != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }

    List<dynamic> data = response.body["data"];

    List<ProductModel> returner = new List();

    for (int x = 0; x < data.length; x++) {
      ProductModel model =
          serializers.deserializeWith(ProductModel.serializer, data[x]);
      returner.add(model);
    }

    return returner;
  }

  static Future<List<ProductModel>> getAllProducts({int from, int to,}) async {
    Response response = await ProductService.create().getAllProducts(
      from:from,
      to:to,
    );

    if (response.body["status"] != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }

    List<dynamic> data = response.body["data"];

    List<ProductModel> returner = new List();

    for (int x = 0; x < data.length; x++) {
      ProductModel model = serializers.deserializeWith(
        ProductModel.serializer,
        data[x],
      );
      returner.add(model);
    }

    return returner;
  }

  static Future<ProductModel> getProductDetails({int productId}) async {
    Response response = await ProductService.create()
        .getProductDetails(productId: productId.toString());

    if (response.body["status"] != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }

    return serializers.deserializeWith(
        ProductModel.serializer, response.body["data"]);
  }

  static Future<void> likeProduct({int productId}) async {
    Response response = await ProductService.create()
        .likeProduct(productId: productId.toString());

    if (response.body["status"] != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }
  }

  static Future<void> dislikeProduct({int productId}) async {
    Response response = await ProductService.create()
        .dislikeProduct(productId: productId.toString());

    if (response.body["status"] != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }
  }

  static Future<List<ProductBrandModel>> getBrands() async {
    Response response = await ProductService.create().getBrands();

    if (response.body["status"] != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }

    List<dynamic> data = response.body["data"];

    List<ProductBrandModel> returner = new List();

    for (int x = 0; x < data.length; x++) {
      ProductBrandModel model =
          serializers.deserializeWith(ProductBrandModel.serializer, data[x]);
      returner.add(model);
    }

    return returner;
  }

  static Future<List<dynamic>> getSizes() async {
    Response response = await ProductService.create().getSizes();

    if (response.body["status"] != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }

    List<dynamic> data = response.body["data"];

    return data;
  }

  static Future<List<dynamic>> getConditions() async {
    Response response = await ProductService.create().getConditions();

    if (response.body["status"] != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }

    List<dynamic> data = response.body["data"];

    return data;
  }

  static Future<List<dynamic>> getGenders() async {
    Response response = await ProductService.create().getGenders();

    if (response.body["status"] != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }

    List<dynamic> data = response.body["data"];

    return data;
  }

  static Future<List<dynamic>> getCurrencies() async {
    Response response = await ProductService.create().getCurrencies();

    if (response.body["status"] != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }

    List<dynamic> data = response.body["data"];

    return data;
  }
}
