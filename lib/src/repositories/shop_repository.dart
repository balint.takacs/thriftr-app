import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:thriftr/src/models/product/product_model.dart';
import 'package:thriftr/src/models/serializers.dart';
import 'package:thriftr/src/models/shop/shop_model.dart';
import 'package:thriftr/src/resources/exception_helper.dart';
import 'package:thriftr/src/services/api/shop/shop_service.dart';

class ShopRepository {
  static Future<void> followShop({int shopId}) async {
    Response response =
        await ShopService.create().followShop(shopId: shopId.toString());

    if (response.body["status"] != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }
  }

  static Future<void> unFollowShop({int shopId}) async {
    Response response =
        await ShopService.create().unFollowShop(shopId: shopId.toString());

    if (response.body["status"] != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }
  }

  static Future<ShopModel> getShopDetails({int shopId}) async {
    Response response =
        await ShopService.create().getShopDetails(shopId: shopId.toString());

    if (response.body["status"] != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }

    return serializers.deserializeWith(
        ShopModel.serializer, response.body["data"]);
  }

  static Future<List<ShopModel>> getAllShops() async {
    Response response = await ShopService.create().getAllShops();

    if (response.body["status"] != 200) {
      throw ExceptionHelper.convertCodeToException(response.body["message"]);
    }

    List<dynamic> data = response.body["data"];
    
    List<ShopModel> returner = new List();

    for (int x = 0; x < data.length; x++) {
      ShopModel model =
          serializers.deserializeWith(ShopModel.serializer, data[x]);
      returner.add(model);
    }

    return returner;
  }

  static Future<List<ShopModel>> getMostVisitedShops() async {
    Response response = await ShopService.create().getMostVisitedShops();

    List<dynamic> data = response.body["data"];
    print(data.runtimeType.toString());
    List<ShopModel> returner = new List();

    for (int x = 0; x < data.length; x++) {
      ShopModel model =
          serializers.deserializeWith(ShopModel.serializer, data[x]);
      returner.add(model);
    }

    return returner;
  }

  static Future<List<ProductModel>> getShopProducts(
      {@required int shopid}) async {
    Response response = await ShopService.create().getShopProducts(
      shopId: shopid.toString(),
    );

    dynamic data = response.body["data"];
    List<ProductModel> returner = new List();

    for (int x = 0; x < data.length; x++) {
      ProductModel model =
          serializers.deserializeWith(ProductModel.serializer, data[x]);
      returner.add(model);
    }
    return returner;
  }
}
