import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:chopper/chopper.dart';
import 'package:thriftr/src/repositories/user_repository.dart';

class TokenInterceptor implements ResponseInterceptor, RequestInterceptor {
  @override
  FutureOr<Request> onRequest(Request request) async {
    String token = await UserRepository.userToken;

    request.parts.forEach(
      (element) {
        print(
          "${element.name} - ${element.value}"
        );
      },
    );
    if (token != null) {
      if (token.isNotEmpty) {
        Map<String, String> _headers = new Map();
        _headers.addAll(
          request.headers,
        );
        _headers.addEntries(
          [
            MapEntry(
              "ThunderToken",
              "$token",
            ),
          ],
        );
        request = request.replace(
          headers: _headers,
        );
      }
    }
    return request;
  }

  @override
  FutureOr<Response> onResponse(Response response) {
    return response;
  }
}
