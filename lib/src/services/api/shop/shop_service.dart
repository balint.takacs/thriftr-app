import 'package:chopper/chopper.dart';
import 'package:thriftr/src/resources/api_helper.dart';
import 'package:thriftr/src/services/converter/built_value_converter.dart';
import 'package:thriftr/src/services/interceptors/token_interceptor.dart';

// this is necessary for the generated code to find your class
part "shop_service.chopper.dart";

@ChopperApi(baseUrl: ApiHelper.baseUrl + "shops")
abstract class ShopService extends ChopperService {
  @Get(path: "/most-visited")
  Future<Response> getMostVisitedShops();

  @Get(path: "/{id}")
  Future<Response> getShopDetails({@Path("id") String shopId});

  @Get(path: "/{id}/products")
  Future<Response> getShopProducts({@Path("id") String shopId});

  @Post(path: "/{id}/follow")
  Future<Response> followShop({@Path("id") String shopId});

  @Post(path: "/{id}/unfollow")
  Future<Response> unFollowShop({@Path("id") String shopId});

  @Post(path: "/search")
  @multipart
  Future<Response> searchInShops({@Part("search") String search});

  @Post()
  @multipart
  Future<Response> getAllShops({
    @Part("from") int from,
    @Part("to") int to,
  });

  static _$ShopService create() {
    final client = ChopperClient(
      baseUrl: ApiHelper.baseUrl,
      converter: BuiltValueConverter(),
      interceptors: [
        TokenInterceptor(),
        HttpLoggingInterceptor(),
      ],
      services: [
        _$ShopService(),
      ],
    );
    return _$ShopService(client);
  }
}
