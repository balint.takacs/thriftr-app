// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shop_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$ShopService extends ShopService {
  _$ShopService([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = ShopService;

  @override
  Future<Response<dynamic>> getMostVisitedShops() {
    final $url = 'https://api.the-thriftr.com/shops/most-visited';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> getShopDetails({String shopId}) {
    final $url = 'https://api.the-thriftr.com/shops/$shopId';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> getShopProducts({String shopId}) {
    final $url = 'https://api.the-thriftr.com/shops/$shopId/products';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> followShop({String shopId}) {
    final $url = 'https://api.the-thriftr.com/shops/$shopId/follow';
    final $request = Request('POST', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> unFollowShop({String shopId}) {
    final $url = 'https://api.the-thriftr.com/shops/$shopId/unfollow';
    final $request = Request('POST', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> searchInShops({String search}) {
    final $url = 'https://api.the-thriftr.com/shops/search';
    final $parts = <PartValue>[PartValue<String>('search', search)];
    final $request =
        Request('POST', $url, client.baseUrl, parts: $parts, multipart: true);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> getAllShops({int from, int to}) {
    final $url = 'https://api.the-thriftr.com/shops';
    final $parts = <PartValue>[
      PartValue<int>('from', from),
      PartValue<int>('to', to)
    ];
    final $request =
        Request('POST', $url, client.baseUrl, parts: $parts, multipart: true);
    return client.send<dynamic, dynamic>($request);
  }
}
