import 'package:chopper/chopper.dart';
import 'package:thriftr/src/resources/api_helper.dart';
import 'package:thriftr/src/services/converter/built_value_converter.dart';
import 'package:thriftr/src/services/interceptors/token_interceptor.dart';

// this is necessary for the generated code to find your class
part "user_service.chopper.dart";

@ChopperApi(baseUrl: ApiHelper.baseUrl + "user")
abstract class UserService extends ChopperService {

  @Get(path: "/details")
  Future<Response> getCurrentUser(); 

  static _$UserService create() {
    final client = ChopperClient(
      baseUrl: ApiHelper.baseUrl,
      converter: BuiltValueConverter(),
      interceptors: [
        TokenInterceptor(),
        HttpLoggingInterceptor(),
      ],
      services: [
        _$UserService(),
      ],
    );
    return _$UserService(client);
  }
}
