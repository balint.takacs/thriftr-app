// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'authentication_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$AuthenticationService extends AuthenticationService {
  _$AuthenticationService([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = AuthenticationService;

  @override
  Future<Response<dynamic>> login({String email, String password}) {
    final $url = 'https://api.the-thriftr.com/authentication/login';
    final $parts = <PartValue>[
      PartValue<String>('email', email),
      PartValue<String>('password', password)
    ];
    final $request =
        Request('POST', $url, client.baseUrl, parts: $parts, multipart: true);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> forgotPassword({String email}) {
    final $url = 'https://api.the-thriftr.com/authentication/forgot-password';
    final $parts = <PartValue>[PartValue<String>('email', email)];
    final $request =
        Request('POST', $url, client.baseUrl, parts: $parts, multipart: true);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> registration(
      {String email, String firstName, String lastName, String password}) {
    final $url = 'https://api.the-thriftr.com/authentication/registration';
    final $parts = <PartValue>[
      PartValue<String>('email', email),
      PartValue<String>('firstName', firstName),
      PartValue<String>('lastName', lastName),
      PartValue<String>('password', password)
    ];
    final $request =
        Request('POST', $url, client.baseUrl, parts: $parts, multipart: true);
    return client.send<dynamic, dynamic>($request);
  }
}
