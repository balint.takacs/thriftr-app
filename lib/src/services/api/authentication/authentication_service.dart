
import 'package:chopper/chopper.dart'; 
import 'package:thriftr/src/resources/api_helper.dart';
import 'package:thriftr/src/resources/network_helper.dart';
import 'package:thriftr/src/services/converter/built_value_converter.dart';
import 'package:thriftr/src/services/interceptors/token_interceptor.dart';

// this is necessary for the generated code to find your class
part "authentication_service.chopper.dart";

@ChopperApi(baseUrl: ApiHelper.baseUrl + "authentication")
abstract class AuthenticationService extends ChopperService {

  @Post(path: "/login")
  @multipart
  Future<Response> login({
    @Part("email") String email,
    @Part("password") String password,
  });

  @Post(path: "/forgot-password")
  @multipart
  Future<Response> forgotPassword({
    @Part("email") String email,
  });

  @Post(path: "/registration")
  @multipart
  Future<Response> registration({
    @Part("email") String email,
    @Part("firstName") String firstName,
    @Part("lastName") String lastName,
    @Part("password") String password, 
  });

  static _$AuthenticationService create() {
    final client = ChopperClient(
      baseUrl: ApiHelper.baseUrl,
      client: NetworkHelper.ioClient,
      converter: BuiltValueConverter(),
      interceptors: [
        HttpLoggingInterceptor(),
      ],
      services: [
        _$AuthenticationService(),
      ],
    );
    return _$AuthenticationService(client);
  }
}
