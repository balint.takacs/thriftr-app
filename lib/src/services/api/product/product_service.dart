import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:thriftr/src/resources/api_helper.dart';
import 'package:thriftr/src/services/converter/built_value_converter.dart';
import 'package:thriftr/src/services/interceptors/token_interceptor.dart';

// this is necessary for the generated code to find your class
part "product_service.chopper.dart";

@ChopperApi(baseUrl: ApiHelper.baseUrl + "products")
abstract class ProductService extends ChopperService {

  @Get(path: "/most-visited")
  Future<Response> getMostPopularProducts(); 
  
  @Get(path: "/{id}")
  Future<Response> getProductDetails({@Path("id") String productId}); 
  
  @Post(path: "/{id}/like")
  Future<Response> likeProduct({@Path("id") String productId}); 
  
  @Post(path: "/{id}/dislike")
  Future<Response> dislikeProduct({@Path("id") String productId}); 

  @Get(path:"/data/brands")
  Future<Response> getBrands(); 
  
  @Get(path:"/data/sizes")
  Future<Response> getSizes(); 

  @Get(path:"/data/conditions")
  Future<Response> getConditions(); 

  @Get(path:"/data/genders")
  Future<Response> getGenders(); 

  @Get(path:"/data/currencies")
  Future<Response> getCurrencies(); 

  @Post(path: "/search")
  @multipart
  Future<Response> searchInProducts({@Part("search") String search}); 

  @Post()
  @multipart
  Future<Response> getAllProducts(
    {
      @Part("from") int from,
      @Part("to") int to,
    }
  ); 

  static _$ProductService create() {
    final client = ChopperClient(
      baseUrl: ApiHelper.baseUrl,
      converter: BuiltValueConverter(),
      interceptors: [
        TokenInterceptor(),
        HttpLoggingInterceptor(),
      ],
      services: [
        _$ProductService(),
      ],
    );
    return _$ProductService(client);
  }
}
