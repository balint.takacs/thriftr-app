// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$ProductService extends ProductService {
  _$ProductService([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = ProductService;

  @override
  Future<Response<dynamic>> getMostPopularProducts() {
    final $url = 'https://api.the-thriftr.com/products/most-visited';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> getProductDetails({String productId}) {
    final $url = 'https://api.the-thriftr.com/products/$productId';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> likeProduct({String productId}) {
    final $url = 'https://api.the-thriftr.com/products/$productId/like';
    final $request = Request('POST', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> dislikeProduct({String productId}) {
    final $url = 'https://api.the-thriftr.com/products/$productId/dislike';
    final $request = Request('POST', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> getBrands() {
    final $url = 'https://api.the-thriftr.com/products/data/brands';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> getSizes() {
    final $url = 'https://api.the-thriftr.com/products/data/sizes';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> getConditions() {
    final $url = 'https://api.the-thriftr.com/products/data/conditions';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> getGenders() {
    final $url = 'https://api.the-thriftr.com/products/data/genders';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> getCurrencies() {
    final $url = 'https://api.the-thriftr.com/products/data/currencies';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> searchInProducts({String search}) {
    final $url = 'https://api.the-thriftr.com/products/search';
    final $parts = <PartValue>[PartValue<String>('search', search)];
    final $request =
        Request('POST', $url, client.baseUrl, parts: $parts, multipart: true);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> getAllProducts({int from, int to}) {
    final $url = 'https://api.the-thriftr.com/products';
    final $parts = <PartValue>[
      PartValue<int>('from', from),
      PartValue<int>('to', to)
    ];
    final $request =
        Request('POST', $url, client.baseUrl, parts: $parts, multipart: true);
    return client.send<dynamic, dynamic>($request);
  }
}
