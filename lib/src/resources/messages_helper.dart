import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:thriftr/src/exceptions/authentication/facebook_sign_in_error_exception.dart';
import 'package:thriftr/src/exceptions/authentication/user_already_registered_exception.dart';
import 'package:thriftr/src/exceptions/authentication/wrong_email_exception.dart';
import 'package:thriftr/src/exceptions/authentication/wrong_password_exception.dart'; 
import 'package:thriftr/src/exceptions/permission/location/no_location_permission_exception.dart';

class MessagesHelper {
  static String succesMessage(BuildContext context,{@required String action}){
    return "Sikeres ${action}...";
  }
  static String getMessageByException(
    BuildContext context, {
    Exception exception,
  }) {
    switch (exception.runtimeType) {
      case WrongEmailException:
        return "Rossz E-Mail cím...";
      case UserAlreadyRegistered:
        return "Felhasználó már regisztrálva van...";
      case WrongPasswordException:
        return "Helytelen jelszó...";
      case FacebookSignInErrorException:
        return "Sikertelen Facebook bejelentkezés...";
      case NoLocationPermissionException:
        return "Ehhez a funkcióhoz engedélyt kell adnia a Hely meghatározáshoz!";
      case LocationServiceDisabledException:
        return "Ehhez a funckióhoz kérjük kapcsolja be a Hely meghatározást!";
      default:
        return "Ismeretlen hiba...";
    }
  }
}
