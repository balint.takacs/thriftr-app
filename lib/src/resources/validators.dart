import 'package:flutter/material.dart';

class Validators {
  static String _emailPattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

  static bool emailIsValid(String email) {
    return RegExp(_emailPattern).hasMatch(email);
  }

  static FormFieldValidator<String> passwordValidator(
    BuildContext context,) {
    
    return (value) {
      if (value.isEmpty) {
        return "Nem lehet üres...";
      }
      if (value.length < 5) {
        return "Jelszó túl rövid...";
      }

      return null;
    };
  }

  static FormFieldValidator<String> passwordMatchValidator(
    BuildContext context, {
    String confirmPassword,
  }) {
    return (value) {
      if (value.isEmpty) {
        return "Üres a mező...";
      }
      if (value.length < 5) {
        return "Jelszó túl rövid...";
      }

      print(value + " - " + confirmPassword);
      if (value != confirmPassword) {
        return "Két jelszó nem egyezik...";
      }
      

      return null;
    };
  }

  static FormFieldValidator<String> nameValidator(
    BuildContext context,
  ) {
    return (value) {
      if (value.isEmpty) {
        return "Nem lehet üres a mező.";
      }
      return null;
    };
  }
  static FormFieldValidator<String> emailValidator(
    BuildContext context,
  ) {
    return (value) {
      if (emailIsValid(value)) {
        return null;
      } else {
        return "Helytelen formátum...";
      }
    };
  }
}
