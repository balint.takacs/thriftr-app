

import 'package:flutter/material.dart';

extension BouncingScroll on SingleChildScrollView {
  SingleChildScrollView normal({@required Widget child}) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: child,
    );
  }
}