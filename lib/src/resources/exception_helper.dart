import 'package:geolocator/geolocator.dart';
import 'package:thriftr/src/exceptions/authentication/facebook_sign_in_error_exception.dart';
import 'package:thriftr/src/exceptions/authentication/user_already_registered_exception.dart';
import 'package:thriftr/src/exceptions/authentication/wrong_email_exception.dart';
import 'package:thriftr/src/exceptions/authentication/wrong_password_exception.dart'; 
import 'package:thriftr/src/exceptions/permission/location/no_location_permission_exception.dart';
import 'package:thriftr/src/exceptions/unkown_error_exception.dart';

class ExceptionHelper {
  static Exception convertCodeToException(String errorCode) {
    switch (errorCode) {
      case "wrong password":
        return WrongPasswordException();
        
      case "user already registered":
        return UserAlreadyRegistered();

      case "wrong email":
        return WrongEmailException();
        
      case "fb_error":
        return FacebookSignInErrorException();

      case "no_location_permission":
        return NoLocationPermissionException();
        
      case "location_service_disabled":
        return LocationServiceDisabledException();

      default:
        return UnkownError();
    }
  }
} 
