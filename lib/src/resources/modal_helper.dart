import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:thriftr/src/resources/style.dart';

class ModalHelper {
  static Future<dynamic> showDatePicker(BuildContext context) async {
    return await showModalBottomSheet(
      context: context,
      builder: (BuildContext ctx) {},
    );
  }

  static Future<dynamic> showPopUp(
    BuildContext context, {
    @required Widget title,
    @required Widget content,
  }) async {
    return await showDialog(
      context: context,
      child: AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            10,
          ),
        ),
        contentPadding: EdgeInsets.all(
          0,
        ),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              decoration: BoxDecoration(
              color: ApplicationStyle.primaryColor,
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(10,),
                ),
              ),
              padding: EdgeInsets.all(0),
              child: Row(
                children: [
                  IgnorePointer(
                    child: CloseButton(
                      color: Colors.transparent,
                      onPressed: () {},
                    ),
                  ),
                  Spacer(),
                  DefaultTextStyle(
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      color: Colors.white,
                    ),
                    child: title,
                  ),
                  Spacer(),
                  CloseButton(
                    color: Colors.white,
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                vertical: 20,
                horizontal: 10,
              ),
              child: content,
            ),
          ],
        ),
      ),
    );
  }

  static Future<dynamic> showCustomModal(
    BuildContext context, {
    Widget title,
    Widget content,
  }) async {
    return await showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(10),
        ),
      ),
      context: context,
      builder: (c) {
        return Padding(
          padding: EdgeInsets.all(
            20,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: DefaultTextStyle(
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                  child: title,
                ),
              ),
              Divider(),
              content,
            ],
          ),
        );
      },
    );
  }
}
