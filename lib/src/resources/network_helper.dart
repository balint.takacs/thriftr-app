import 'dart:io';

import 'package:http/io_client.dart';

class NetworkHelper{

  static IOClient get ioClient {
    HttpClient webHttpClient = new HttpClient();
    webHttpClient.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true); 
    return new IOClient(webHttpClient);
  }
  
}