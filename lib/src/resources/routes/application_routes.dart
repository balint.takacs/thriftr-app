import 'package:auto_route/auto_route_annotations.dart';
import 'package:thriftr/src/ui/pages/authentication/login_page.dart';
import 'package:thriftr/src/ui/pages/authentication/registration_page.dart';
import 'package:thriftr/src/ui/pages/bussines/all_bussines_page.dart';
import 'package:thriftr/src/ui/pages/bussines/create_bussines_page.dart';
import 'package:thriftr/src/ui/pages/bussines/edit_bussines_page.dart';
import 'package:thriftr/src/ui/pages/bussines/view_bussines_page.dart';
import 'package:thriftr/src/ui/pages/product/all_products_page.dart';
import 'package:thriftr/src/ui/pages/product/view_product_page.dart';
import 'package:thriftr/src/ui/pages/root_page.dart';

export 'application_routes.gr.dart';

@MaterialAutoRouter(routes: [

  MaterialRoute(
    page: RootPage,
    initial: true,
    path: "/"
  ),
  
  MaterialRoute(
    page: LoginPage, 
    path: "/authentication/login"
  ), 
  MaterialRoute(
    page: RegistrationPage, 
    path: "/authentication/registration"
  ),

  MaterialRoute(
    page: AllBussinesPage,
    path: "/bussines/all"
  ),
  MaterialRoute(
    page: ViewBussinesPage, 
    path: "/bussines/view"
  ),
  MaterialRoute(
    page: EditBussinesPage, 
    path: "/bussines/edit"
  ),
  MaterialRoute(
    page: CreateBussinesPage, 
    path: "/bussines/create"
  ),
  
  MaterialRoute(
    page: AllProductsPage, 
    path: "/product/all"
  ),
  MaterialRoute(
    page: ViewProductPage, 
    path: "/product/view"
  ),

])
class $Router {}
