// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../../models/product/product_model.dart';
import '../../models/shop/shop_model.dart';
import '../../ui/pages/authentication/login_page.dart';
import '../../ui/pages/authentication/registration_page.dart';
import '../../ui/pages/bussines/all_bussines_page.dart';
import '../../ui/pages/bussines/create_bussines_page.dart';
import '../../ui/pages/bussines/edit_bussines_page.dart';
import '../../ui/pages/bussines/view_bussines_page.dart';
import '../../ui/pages/product/all_products_page.dart';
import '../../ui/pages/product/view_product_page.dart';
import '../../ui/pages/root_page.dart';

class Routes {
  static const String rootPage = '/';
  static const String loginPage = '/authentication/login';
  static const String registrationPage = '/authentication/registration';
  static const String allBussinesPage = '/bussines/all';
  static const String viewBussinesPage = '/bussines/view';
  static const String editBussinesPage = '/bussines/edit';
  static const String createBussinesPage = '/bussines/create';
  static const String allProductsPage = '/product/all';
  static const String viewProductPage = '/product/view';
  static const all = <String>{
    rootPage,
    loginPage,
    registrationPage,
    allBussinesPage,
    viewBussinesPage,
    editBussinesPage,
    createBussinesPage,
    allProductsPage,
    viewProductPage,
  };
}

class Router extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.rootPage, page: RootPage),
    RouteDef(Routes.loginPage, page: LoginPage),
    RouteDef(Routes.registrationPage, page: RegistrationPage),
    RouteDef(Routes.allBussinesPage, page: AllBussinesPage),
    RouteDef(Routes.viewBussinesPage, page: ViewBussinesPage),
    RouteDef(Routes.editBussinesPage, page: EditBussinesPage),
    RouteDef(Routes.createBussinesPage, page: CreateBussinesPage),
    RouteDef(Routes.allProductsPage, page: AllProductsPage),
    RouteDef(Routes.viewProductPage, page: ViewProductPage),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    RootPage: (data) {
      final args = data.getArgs<RootPageArguments>(
        orElse: () => RootPageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => RootPage(key: args.key),
        settings: data,
      );
    },
    LoginPage: (data) {
      final args = data.getArgs<LoginPageArguments>(
        orElse: () => LoginPageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => LoginPage(key: args.key),
        settings: data,
      );
    },
    RegistrationPage: (data) {
      final args = data.getArgs<RegistrationPageArguments>(
        orElse: () => RegistrationPageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => RegistrationPage(key: args.key),
        settings: data,
      );
    },
    AllBussinesPage: (data) {
      final args = data.getArgs<AllBussinesPageArguments>(
        orElse: () => AllBussinesPageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => AllBussinesPage(key: args.key),
        settings: data,
      );
    },
    ViewBussinesPage: (data) {
      final args = data.getArgs<ViewBussinesPageArguments>(
        orElse: () => ViewBussinesPageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => ViewBussinesPage(
          key: args.key,
          shopModel: args.shopModel,
        ),
        settings: data,
      );
    },
    EditBussinesPage: (data) {
      final args = data.getArgs<EditBussinesPageArguments>(
        orElse: () => EditBussinesPageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => EditBussinesPage(key: args.key),
        settings: data,
      );
    },
    CreateBussinesPage: (data) {
      final args = data.getArgs<CreateBussinesPageArguments>(
        orElse: () => CreateBussinesPageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => CreateBussinesPage(key: args.key),
        settings: data,
      );
    },
    AllProductsPage: (data) {
      final args = data.getArgs<AllProductsPageArguments>(
        orElse: () => AllProductsPageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => AllProductsPage(key: args.key),
        settings: data,
      );
    },
    ViewProductPage: (data) {
      final args = data.getArgs<ViewProductPageArguments>(
        orElse: () => ViewProductPageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => ViewProductPage(
          key: args.key,
          productModel: args.productModel,
        ),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// RootPage arguments holder class
class RootPageArguments {
  final Key key;
  RootPageArguments({this.key});
}

/// LoginPage arguments holder class
class LoginPageArguments {
  final Key key;
  LoginPageArguments({this.key});
}

/// RegistrationPage arguments holder class
class RegistrationPageArguments {
  final Key key;
  RegistrationPageArguments({this.key});
}

/// AllBussinesPage arguments holder class
class AllBussinesPageArguments {
  final Key key;
  AllBussinesPageArguments({this.key});
}

/// ViewBussinesPage arguments holder class
class ViewBussinesPageArguments {
  final Key key;
  final ShopModel shopModel;
  ViewBussinesPageArguments({this.key, this.shopModel});
}

/// EditBussinesPage arguments holder class
class EditBussinesPageArguments {
  final Key key;
  EditBussinesPageArguments({this.key});
}

/// CreateBussinesPage arguments holder class
class CreateBussinesPageArguments {
  final Key key;
  CreateBussinesPageArguments({this.key});
}

/// AllProductsPage arguments holder class
class AllProductsPageArguments {
  final Key key;
  AllProductsPageArguments({this.key});
}

/// ViewProductPage arguments holder class
class ViewProductPageArguments {
  final Key key;
  final ProductModel productModel;
  ViewProductPageArguments({this.key, this.productModel});
}
