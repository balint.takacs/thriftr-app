import 'dart:developer';

import 'package:flutter/material.dart';

class LoadingInterface with ChangeNotifier {
  bool _isLoading = false;
  bool get isLoading {
    return _isLoading;
  }

  set isLoading(value) {
    _isLoading = value;
    log("Notified.");
    notifyListeners();
  }
}
