class ProductHelper{
  static const int boxPrice = 160;

  static bool hasBoxPrice(String category){
    
    if(category == null){
      throw Exception("[Error]: 'category' parameter is null.");
    }
    return category == "Pasta" || category == "Insalate" || category == "Vapiano Kids Pasta";
  }
}