import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:thriftr/src/resources/modal_helper.dart';
import 'package:thriftr/src/resources/style.dart';

class DateHelper {
  static Future<DateTime> pickDate(BuildContext context) async {
    int startHour = DateTime.now().hour + 1;
    int maxHour = 19;

    List<int> hours = new List();
    List<int> minutes = List.generate(59, (d) => d);

    for (int x = startHour; x < maxHour; x++) {
      hours.add(x + 1);
    }
    int selectedHour;
    int selectedMinute;

    await ModalHelper.showCustomModal(
      context,
      title: Row(
        children: [
          InkWell(
            onTap: () {
              selectedHour = null;
              selectedMinute = null;
              Navigator.of(context).pop();
            },
            child: Text(
              "Mégsem",
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 16,
              ),
            ),
          ),
          Spacer(),
          InkWell(
            onTap: () {
              if (selectedHour == null) {
                selectedHour = hours.first;
              }
              if (selectedMinute == null) {
                selectedMinute = minutes.first + 1;
              }
              Navigator.of(context).pop();
            },
            child: Text(
              "Kész",
              style: TextStyle(
                color: ApplicationStyle.primaryColor,
                fontWeight: FontWeight.normal,
                fontSize: 16,
              ),
            ),
          ),
        ],
      ),
      content: Container(
        height: 200,
        child: Stack(
          children: [
            Positioned.fill(
              child: Row(
                children: [
                  Expanded(
                    child: ListWheelScrollView( 
                      physics: BouncingScrollPhysics(),
                      itemExtent: 25,
                      diameterRatio: 1,
                      useMagnifier: true,
                      magnification: 1.5,
                      onSelectedItemChanged: (data) {
                        print("Hour:$data");
                        selectedHour = hours[data];
                      },
                      children: hours
                          .map(
                            (e) => Text(
                              "$e óra",
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              ),
                            ),
                          )
                          .toList(),
                    ),
                  ),
                  Expanded(
                    child: ListWheelScrollView( 
                      physics: BouncingScrollPhysics(),
                      itemExtent: 25,
                      diameterRatio: 1,
                      useMagnifier: true,
                      magnification: 1.5,
                      onSelectedItemChanged: (data) {
                        selectedMinute = minutes[data] + 1;
                      },
                      children: minutes
                          .map(
                            (e) => Text(
                              "${e + 1} perc",
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              ),
                            ),
                          )
                          .toList(),
                    ),
                  ),
                ],
              ),
            ),
            Positioned.fill(
              child: IgnorePointer(
                child: Center(
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          height: 40,
                          decoration: BoxDecoration(
                            border: Border.symmetric(
                              horizontal: BorderSide(
                                color: Colors.black26,
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 40),
                      Expanded(
                        child: Container(
                          height: 40,
                          decoration: BoxDecoration(
                            border: Border.symmetric(
                              horizontal: BorderSide(
                                color: Colors.black26,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );

    if (selectedHour != null && selectedMinute != null) {
      DateTime date = DateTime(
        DateTime.now().year,
        DateTime.now().month,
        DateTime.now().day,
        selectedHour,
        selectedMinute,
      );
      return date;
    }
  }

  static String formatDuration(Duration d) {
    var seconds = d.inSeconds;
    final days = seconds ~/ Duration.secondsPerDay;
    seconds -= days * Duration.secondsPerDay;
    final hours = seconds ~/ Duration.secondsPerHour;
    seconds -= hours * Duration.secondsPerHour;
    final minutes = seconds ~/ Duration.secondsPerMinute;
    seconds -= minutes * Duration.secondsPerMinute;

    final List<String> tokens = [];
    if (days != 0) {
      tokens.add('${days}');
    }
    if (tokens.isNotEmpty || hours != 0) {
      tokens.add('${hours}');
    }
    if (tokens.isNotEmpty || minutes != 0) {
      if (minutes < 10) {
        tokens.add("0${minutes}");
      } else {
        tokens.add('${minutes}');
      }
    }

    if (seconds < 10) {
      tokens.add("0${seconds}");
    } else {
      tokens.add('${seconds}');
    }

    return tokens.join(':');
  }
}
