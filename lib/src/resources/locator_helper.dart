import 'package:geolocator/geolocator.dart';
import 'package:thriftr/src/exceptions/permission/location/no_location_permission_exception.dart';

class LocatorHelper {
  static Future<void> openSettings() async {
    await Geolocator.openLocationSettings();
  }

  static Future<void> requestPermission() async {
    LocationPermission permission = await Geolocator.checkPermission();
    
    try {
      if(permission == LocationPermission.deniedForever){
        await openSettings();
        return;
      }
      return await Geolocator.requestPermission();
    } catch (e) {
      await openSettings();
    }
  }

  static Future<Position> getPosition() async {
    LocationPermission permission = await Geolocator.checkPermission();
    bool enabled = await Geolocator.isLocationServiceEnabled();

    if (!enabled) {
      openSettings();
      throw LocationServiceDisabledException();
    }
    if (permission == LocationPermission.denied ||
        permission == LocationPermission.deniedForever) {
      requestPermission();
      throw NoLocationPermissionException();
    }

    return await Geolocator.getCurrentPosition( 
    );
  }
}
