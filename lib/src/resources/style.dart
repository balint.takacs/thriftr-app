import 'package:flutter/material.dart';

class ApplicationStyle {
  static InputBorder get inputBorder {
    return UnderlineInputBorder(
      borderSide: BorderSide(
          color: Colors.black26,
      ),
    );
  }

  static InputBorder get errorInputBorder {
    return UnderlineInputBorder( 
      borderSide: BorderSide(
        color:primaryColor,
      ),
    );
  }

  static double get paddingValue {
    return 20;
  }

  static Color get primaryColor {
    return Color(0xFF262626);
  }

  static String get stylePath {
    return "assets/style/";
  }

  static ThemeData get themeData {
    return ThemeData(
      primaryColor: primaryColor,
      dividerColor: Colors.transparent,
      fontFamily: "Montserrat",
      scaffoldBackgroundColor: Colors.white,
      textTheme: TextTheme(
        subtitle1: TextStyle(),
      ),
      appBarTheme: AppBarTheme(
        color: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(
          color: primaryColor
        ),
        textTheme: TextTheme(
          headline6: TextStyle(
            color: Colors.black,
            fontFamily: "Montserrat",
            fontSize: 20,
          ),
        ),
      ),
      cardTheme: CardTheme(
        elevation: 5,
        shadowColor: Colors.black.withOpacity(
          0.2,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(
              10,
            ),
          ),
        ),
      ),
      buttonTheme: ButtonThemeData(
        buttonColor: primaryColor,
        textTheme: ButtonTextTheme.primary,
        padding: EdgeInsets.symmetric(vertical:15, horizontal:15,),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            0,
          ),
        ),
      ),
      cursorColor: primaryColor,
      inputDecorationTheme: InputDecorationTheme(
        hintStyle: TextStyle(
          color: Colors.black26,
        ), 
        contentPadding: EdgeInsets.symmetric(
          vertical: 5,
          horizontal: 5,
        ), 
        border: inputBorder,
        disabledBorder: inputBorder,
        enabledBorder: inputBorder,
        focusedBorder: inputBorder.copyWith(borderSide: BorderSide(
          color: primaryColor,
        )),
        errorBorder: errorInputBorder,
        focusedErrorBorder: errorInputBorder,
      ),
    );
  }
}
