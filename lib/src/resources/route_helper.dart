import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:thriftr/src/resources/routes/application_routes.dart';

class RouteHelper {
  static final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();
      

  static Future<dynamic> pushNamed(
    BuildContext context, {
    String routeName,
    Object arguments,
  }) async {
    return await ExtendedNavigator.of(context).push(
      routeName,
      arguments: arguments,
    );
  }
}
